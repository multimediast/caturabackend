package za.co.util;

import java.awt.Color;
import java.awt.Font;

public class GlobalSettings {

	// IMAGES
	public static String CATURABACKEND_IMAGES_PATH = "C:\\Project Images\\CaturaBackEndIcons";

	public static String CATURABACKEND_CATURA_LOGO = CATURABACKEND_IMAGES_PATH + "\\caturalogo.png";
	public static String CATURABACKEND_WHITE_LOGO = CATURABACKEND_IMAGES_PATH + "\\caturalogowhitelogo.png";
	public static String CATURABACKEND_USER_ICON = CATURABACKEND_IMAGES_PATH + "\\iconUser.png";
	public static String CATURABACKEND_PASS_ICON = CATURABACKEND_IMAGES_PATH + "\\lockIcon.png";
	
	
	public static String CATURABACKEND_SEARCH_ICON = CATURABACKEND_IMAGES_PATH + "\\search.png";
	public static String CATURABACKEND_ADD_ICON = CATURABACKEND_IMAGES_PATH + "\\add.png";
	public static String CATURABACKEND_EDIT_ICON = CATURABACKEND_IMAGES_PATH + "\\edit.png";
	public static String CATURABACKEND_ACCOUNT_ICON = CATURABACKEND_IMAGES_PATH + "\\account.png";

	// PANEL COLORS
	public static Color PANEL_BLUE = new Color(85, 142, 203);
	public static Color PANEL_WHITE = new Color(255, 255, 255);

	// BUTTONS COLORS:
	public static Color BUTTON_ORANGE = new Color(246, 141, 89);
	public static Color BUTTON_WHITE = new Color(255, 255, 255);
	public static Color BUTTON_FOREGROUND_WHITE = new Color(255, 255, 255);

	// LABELS COLORS:
	public static Color LBL_BLACK = new Color(0, 0, 0);
	public static Color LBL_WHITE = new Color(255, 255, 255);

	// FONT SIZE
	public static String TEXT_TYPE = "Verdana";
	public static int TEXT_SIZE_10 = 10;
	public static int TEXT_SIZE_12 = 12;
	public static int TEXT_SIZE_16 = 16;
	public static int TEXT_SIZE_24 = 24;

	public static Font VERDANA_BOLD_10 = new Font(TEXT_TYPE, Font.BOLD, TEXT_SIZE_10);
	public static Font VERDANA_PLAIN = new Font(TEXT_TYPE, Font.PLAIN, TEXT_SIZE_12);
	public static Font VERDANA_BOLD_12 = new Font(TEXT_TYPE, Font.BOLD, TEXT_SIZE_12);
	public static Font VERDANA_BOLD_16 = new Font(TEXT_TYPE, Font.BOLD, TEXT_SIZE_16);
	public static Font VERDANA_BOLD_24 = new Font(TEXT_TYPE, Font.BOLD, TEXT_SIZE_24);

}
