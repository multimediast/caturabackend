package za.co.maintainproduct;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;
import org.restlet.data.MediaType;
import org.restlet.ext.xml.SaxRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import za.co.main.Menu;
import za.co.smartpharmalib.datasource.ActiveIngredients;
import za.co.smartpharmalib.datasource.ActiveIngredientsDao;
import za.co.smartpharmalib.datasource.ActiveIngredientsImpl;
import za.co.smartpharmalib.datasource.BonusClass;
import za.co.smartpharmalib.datasource.BonusClassDao;
import za.co.smartpharmalib.datasource.BonusClassImpl;
import za.co.smartpharmalib.datasource.Ingredient;
import za.co.smartpharmalib.datasource.IngredientDao;
import za.co.smartpharmalib.datasource.IngredientImpl;
import za.co.smartpharmalib.datasource.IngredientStrength;
import za.co.smartpharmalib.datasource.IngredientStrengthDao;
import za.co.smartpharmalib.datasource.IngredientStrengthImpl;
import za.co.smartpharmalib.datasource.Manufacturer;
import za.co.smartpharmalib.datasource.ManufacturerDao;
import za.co.smartpharmalib.datasource.ManufacturerImpl;
import za.co.smartpharmalib.datasource.MimsLevel1;
import za.co.smartpharmalib.datasource.MimsLevel1Dao;
import za.co.smartpharmalib.datasource.MimsLevel1Impl;
import za.co.smartpharmalib.datasource.MimsLevel2;
import za.co.smartpharmalib.datasource.MimsLevel2Dao;
import za.co.smartpharmalib.datasource.MimsLevel2Impl;
import za.co.smartpharmalib.datasource.MimsLevel3;
import za.co.smartpharmalib.datasource.MimsLevel3Dao;
import za.co.smartpharmalib.datasource.MimsLevel3Impl;
import za.co.smartpharmalib.datasource.Product;
import za.co.smartpharmalib.datasource.ProductDao;
import za.co.smartpharmalib.datasource.ProductForm;
import za.co.smartpharmalib.datasource.ProductFormDao;
import za.co.smartpharmalib.datasource.ProductFormImpl;
import za.co.smartpharmalib.datasource.ProductFromXml;
import za.co.smartpharmalib.datasource.ProductHasActiveIngredients;
import za.co.smartpharmalib.datasource.ProductHasActiveIngredientsDao;
import za.co.smartpharmalib.datasource.ProductHasActiveIngredientsImpl;
import za.co.smartpharmalib.datasource.ProductImpl;
import za.co.smartpharmalib.datasource.ProductToXml;
import za.co.smartpharmalib.datasource.StockItem;
import za.co.smartpharmalib.datasource.StockItemBonus;
import za.co.smartpharmalib.datasource.StockItemBonusDao;
import za.co.smartpharmalib.datasource.StockItemBonusImpl;
import za.co.smartpharmalib.datasource.StockItemDao;
import za.co.smartpharmalib.datasource.StockItemImpl;
import za.co.smartpharmalib.datasource.StockItemPrice;
import za.co.smartpharmalib.datasource.StockItemPriceDao;
import za.co.smartpharmalib.datasource.StockItemPriceImpl;
import za.co.smartpharmalib.datasource.StockItemToXml;
import za.co.smartpharmalib.datasource.Supplier;
import za.co.smartpharmalib.datasource.SupplierDao;
import za.co.smartpharmalib.datasource.SupplierImpl;
import za.co.smartutillib.errorlog.ExceptionLogger;
import za.co.smartutillib.util.DataBasePooling;
import za.co.smartutillib.util.GetProperties;
import za.co.smartutillib.util.NumericUtil;
import za.co.util.GlobalSettings;

public class MaintainProduct extends JFrame implements ActionListener, KeyListener {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(MaintainProduct.class.getName());
	private static String AND = "And";

	// private JFrame frm;

	private GetProperties getproperties = new GetProperties();

	private String SERVER_HTTP = getproperties.GetProperty("SERVER_HTTP");
	private static final Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private static final Cursor defaultCursor = Cursor.getDefaultCursor();

	public JFrame pnlProduct;
	private ProductDao productdao = new ProductImpl();
	private Product product = new Product();
	private ManufacturerDao manufacturerdao = new ManufacturerImpl();
	private Manufacturer manufacturer = new Manufacturer();
	private DataSource datasource = null;
	private ProductFromXml productfromxml = new ProductFromXml();

	private StockItemDao stockitemdao = new StockItemImpl();
	private StockItem stockitem = new StockItem();

	private SupplierDao supplierdao = new SupplierImpl();
	private Supplier supplier = new Supplier();

	private ProductHasActiveIngredientsDao producthasactiveingredientsdao = new ProductHasActiveIngredientsImpl();
	private ProductHasActiveIngredients producthasactiveingredients = new ProductHasActiveIngredients();

	private BonusClassDao bonusclassdao = new BonusClassImpl();
	private BonusClass bonusclass = new BonusClass();

	private StockItemBonusDao stockitembonusdao = new StockItemBonusImpl();
	private StockItemBonus stockitembonus = new StockItemBonus();

	private NumericUtil numericutil = new NumericUtil();

	private StockItemToXml stockitemtoxml = new StockItemToXml();

	private ProductFormDao productformdao = new ProductFormImpl();
	private ProductForm productform = new ProductForm();

	private ActiveIngredientsDao activeingredientsdao = new ActiveIngredientsImpl();
	private ActiveIngredients activeingredients = new ActiveIngredients();

	private IngredientDao ingredientdao = new IngredientImpl();
	private Ingredient ingredient = new Ingredient();

	private IngredientStrengthDao ingredientstrengthdao = new IngredientStrengthImpl();
	private IngredientStrength ingredientstrength = new IngredientStrength();

	private StockItemPriceDao stockitempricedao = new StockItemPriceImpl();
	private StockItemPrice stockitemprice = new StockItemPrice();

	private MimsLevel1Dao mimslevel1dao = new MimsLevel1Impl();
	private MimsLevel1 mimslevel1 = new MimsLevel1();

	private MimsLevel2Dao mimslevel2dao = new MimsLevel2Impl();
	private MimsLevel2 mimslevel2 = new MimsLevel2();

	private MimsLevel3Dao mimslevel3dao = new MimsLevel3Impl();
	private MimsLevel3 mimslevel3 = new MimsLevel3();

	/* Constant variables */

	private JSpinner spnrIngPoition = new JSpinner();

	private String[] prodSchedOptions = new String[] { "&", "0", "1", "2", "3", "4", "5", "6", "7" };
	// private String[] productTypeOptions = {"E","S","R"};
	private String[] productTypeOptionsMeaning = { "E = Ethical Products", "EO = Ethical Original",
			"EG = Ethical Generic", "EC = Ethical Clone", "S = Surgical Products", "V = Veterinary Products",
			"O = Other" };

	// private String[] productTypeOptionsMeaning = { "S = Surgical Products",
	// "R = Reference Products" };

	/* Text fields variables declaration */
	private JTextField edtProductID = new JTextField();
	private JTextField edtProductBarcode = new JTextField();
	private JTextField edtProductNappicode = new JTextField();
	private JTextField edtproductName;
	private JTextField edtProductNameSearch1 = new JTextField();
	private JTextField edtProductName2 = new JTextField();
	private JTextField edtProductName3 = new JTextField();
	private JTextField edtProductName4 = new JTextField();
	private JTextField edtProductName5 = new JTextField();
	private JTextField edtCatalogueNum = new JTextField();
	private JTextField edtDosageForm = new JTextField();
	private JTextField edtProductStrength = new JTextField();
	private JTextField edtProductPack = new JTextField();
	private JTextArea edtProductDescrip = new JTextArea();
	private JTextArea edtMyPasteField = new JTextArea();

	/* Combo variables */
	private JComboBox<String> cmbProductSched = new JComboBox<String>();
	private JComboBox<String> cmbProductType = new JComboBox<String>();
	
	private JComboBox<String> cmbSupplierList = new JComboBox<String>();
	private JComboBox<String> cmbProductForm = new JComboBox<String>();
	private JComboBox<String> cmbManufacturer = new JComboBox<String>();
	
	
	// private JComboBox<String> cmbIngredient = new JComboBox<String>();
	// private JComboBox<String> cmbStrength = new JComboBox<String>();
	private JComboBox<String> cmbActiveIngredient = new JComboBox<String>();

	private JComboBox<String> cmbIngredient2 = new JComboBox<String>();
	private JComboBox<String> cmbStrength2 = new JComboBox<String>();

	private JComboBox<String> cmbMimsLevel1 = new JComboBox<String>();
	private JComboBox<String> cmbMimsLevel2 = new JComboBox<String>();
	private JComboBox<String> cmbMimsLevel3 = new JComboBox<String>();
;

	private String[] arrActiveIng;

	// private String[] arrIngredient;
	private List<Ingredient> aIngredientList;
	private List<IngredientStrength> aIngredientStrengthList;
	// private List<ActiveIngredients> aListIdActiveIngrList;
	private List<ActiveIngredients> aActiveIngredientsList;
	private List<ProductHasActiveIngredients> aProductHasIng;

	private List<MimsLevel1> aListMimsLevel1;
	private List<MimsLevel2> aListMimsLevel2;
	private List<MimsLevel3> aListMimsLevel3;

	/* Grid list declaration */
	// private java.awt.List listGrid = null;

	/* variables */
	private String barcode;
	private String nappicode;
	private String name1;
	private String name2;
	private String name3;
	private String name4;
	private String name5;
	// private Boolean productFound = false;
	private List<Product> aProductList = null;

	/* Button option: search,edit,save */
	private JButton btnDelProd = new JButton("Del Prod");
	private JButton btnReLinkStockItem = new JButton("Re-Link");
	private JButton btnAddStockItem = new JButton("Add StockItem");
	private JButton btnIdProdSearch = new JButton("Search");
	private JButton btnClearPastField = new JButton("Clear");
	// private JButton btnNewIngredient = new JButton("");
	// private JButton btnNewStrength = new JButton("");
	private JButton btnAddIngr = new JButton("Add Ingr");
	private JButton btnRemoveIngr = new JButton("Remove Ingr");
	private JButton btnAddClassification = new JButton("Add / Update");
	// private JButton btnUpdateStrenth = new JButton("Update Strenth");
	private JButton btnUpdateStrenth2 = new JButton("Change Strength");

	private JButton btnStartWith = new JButton("Start With");
	private JButton btnNewIngredient2 = new JButton("Add");
	private JButton btnNewStrength2 = new JButton("Add");
	private JButton btnCreateActIngr = new JButton("Create Act Ingr");
	private JButton btnReplaceIngr = new JButton("Replace Ingr");
	private JTable tblSearchResult;
	private JTable tblActiveIngredient;
	private JTable tblIngredientList;
	private JTable tblStockItem;

	private JLabel lblNewDisplayName = new JLabel("New label");

	private DefaultTableModel tableModStockItem = null;
	private DefaultTableModel tableModSearchResult = null;
	private DefaultTableModel tableModelActiveIng = null;
	private DefaultTableModel tableModelActiveIngList = null;

	private TableRowSorter<TableModel> rowSorter = null; // new
															// TableRowSorter<>(tblIngredientList.getModel());

	private List<Supplier> aSuppList = null;
	private JTextField edtStockItemDesc;
	private String[] arSupplierCode;

	private JTextField edtStockItemProdNo = null;

	private List<ProductForm> aProductFormList = null;
	// private List<Manufacturer> aManufacturerList = null;
	private String[] arManufacturer;
	private List<StockItem> aStkList = null;

	private int[] arrIngPos = new int[30];
	private ArrayList<Integer> aListTableIngr = new ArrayList<Integer>();
	private ArrayList<Integer> aListIdActiveIngr = new ArrayList<Integer>();
	private JTextField edtSearchIngr;
	private JPanel panel_36;

	// -----------------------------------------
	/**
	 * Create the application.
	 */
	public MaintainProduct() {
		try {
			String dir = getproperties.GetProperty("DATABASE_2");
			datasource = DataBasePooling.setConnectionNew(dir);

			// String dir = getproperties.GetProperty("DATABASE");
			// datasource = DataBasePooling.setConnection(dir);

		} catch (Exception ex) {
			ExceptionLogger.logException(log, ex);
		}

		getListOfAllSuppliers();
		getListOfManufacturers();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {

		pnlProduct = new JFrame();
		pnlProduct.setBounds(100, 100, 450, 300);
		pnlProduct.setTitle("Maintain Product");
		// width = (int) screenSize.getWidth();
		// height = (int) screenSize.getHeight();
		// height = 730; //fix height screen
		pnlProduct.setSize(1370, 740);
		pnlProduct.setLocationRelativeTo(null);
		pnlProduct.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		pnlProduct.getContentPane().setLayout(new BorderLayout(0, 0));
		btnNewIngredient2.setFont(new Font("Verdana", Font.BOLD, 11));
		btnNewIngredient2.setForeground(new Color(255, 255, 255));
		btnNewIngredient2.setBackground(GlobalSettings.BUTTON_ORANGE);

		// btnNewIngredient.setIcon(new
		// ImageIcon("C:\\CaturaBackEnd\\bin\\add.png"));
		// btnNewIngredient.setBounds(399, 25, 24, 23);
		// btnNewIngredient.addActionListener(this);

		btnNewIngredient2.addActionListener(this);

		btnAddClassification.setBounds(10, 218, 122, 23);
		btnAddClassification.addActionListener(this);

		/* Panels declaration */
		JPanel panel = new JPanel();
		pnlProduct.getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel PnlNorth = new JPanel();
		PnlNorth.setBackground(new Color(211, 211, 211));
		PnlNorth.setBorder(null);
		panel.add(PnlNorth, BorderLayout.NORTH);
		PnlNorth.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel_24 = new JPanel();
		panel_24.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_24.setBackground(GlobalSettings.PANEL_BLUE);
		PnlNorth.add(panel_24);
		panel_24.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panel_34 = new JPanel();
		panel_34.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_15 = (FlowLayout) panel_34.getLayout();
		flowLayout_15.setAlignment(FlowLayout.LEFT);
		panel_24.add(panel_34);

		JLabel lblNewLabel_10 = new JLabel("");
		lblNewLabel_10.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_WHITE_LOGO));
		panel_34.add(lblNewLabel_10);

		JPanel panel_33 = new JPanel();
		panel_33.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_16 = (FlowLayout) panel_33.getLayout();
		flowLayout_16.setAlignment(FlowLayout.RIGHT);
		panel_24.add(panel_33);

		JLabel lblNewLabel_11 = new JLabel("Maintain Product");
		lblNewLabel_11.setForeground(Color.WHITE);
		lblNewLabel_11.setFont(new Font("Verdana", Font.BOLD, 28));
		panel_33.add(lblNewLabel_11);

		JPanel pnlCenter = new JPanel();
		panel.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new BorderLayout(0, 0));

		JPanel pnlSubCenter = new JPanel();
		pnlSubCenter.setBackground(GlobalSettings.PANEL_BLUE);
		pnlCenter.add(pnlSubCenter);

		/* Label declaration */
		final int MAX_LENGTH = 200;

		/* Combo initialization */

		/* Button Initialization */
		/*
		 * defaultTableMod.addRow(new Object[][] { {"WS Code", "WS Prod #",
		 * "Prod WS Name"}, {"A", "B", "C"}, {null, null, null}, {null, null, null},
		 * {null, null, null}, });
		 * 
		 * 
		 */
		
		// ************for development
		//cmbSupplierList = new JComboBox<String>(); // ****drie -development
		//cmbManufacturer = new JComboBox<String>(); // *drie development

		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		// ****************for live

		//cmbSupplierList = new JComboBox<String>(arSupplierCode);
		//cmbManufacturer = new JComboBox<String>(arManufacturer);
		cmbSupplierList.setModel(new DefaultComboBoxModel<String>(arSupplierCode));
		cmbManufacturer.setModel(new DefaultComboBoxModel<String>(arManufacturer));
		

		JPanel pnlSearchResult = new JPanel();
		pnlSearchResult.setBackground(GlobalSettings.PANEL_BLUE);
		pnlSearchResult.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlSearchResult.setBounds(5, 355, 785, 250);

		// table
		// tblSearchResult = new JTable();

		String[] col = {"Prod Name", "Pack", "Dosage", "Strength", "Manufacturer", "NAPPI", "Barcode", "id" };
		tableModSearchResult = new DefaultTableModel(col, 14);
		// tblSearchResult = new JTable(tableModSearchResult);
		tblSearchResult = new JTable(tableModSearchResult) {
			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(final TableCellRenderer renderer, final int row, final int column) {
				Component comp = super.prepareRenderer(renderer, row, column);
				// int modelRow = convertRowIndexToModel(row);
				Object id = getValueAt(row, 7);
				comp.setForeground(Color.BLACK);
				if (id != null) {
					int i = (Integer) getValueAt(row, 7);
					if (i == 0) {
						comp.setForeground(Color.GREEN);
					}
				}

				return comp;
			}
		};

		JTableHeader anHeader = tblSearchResult.getTableHeader();
		anHeader.setForeground(Color.BLUE);
		anHeader.setBackground(Color.red);

		tblSearchResult.setRowSelectionAllowed(true);
		tblSearchResult.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblSearchResult.setOpaque(false);
		tblSearchResult.getColumnModel().getColumn(0).setPreferredWidth(600);
		tblSearchResult.getColumnModel().getColumn(2).setPreferredWidth(150);
		tblSearchResult.getColumnModel().getColumn(3).setPreferredWidth(150);
		tblSearchResult.getColumnModel().getColumn(4).setPreferredWidth(550);
		tblSearchResult.getColumnModel().getColumn(5).setPreferredWidth(150);
		tblSearchResult.getColumnModel().getColumn(6).setPreferredWidth(250);

		tblSearchResult.getColumnModel().getColumn(7).setMinWidth(0);
		tblSearchResult.getColumnModel().getColumn(7).setMaxWidth(0);
		pnlSearchResult.setLayout(new GridLayout(0, 1, 0, 0));

		tblSearchResult.setShowVerticalLines(true);

		// tblSearchResult.setAutoCreateColumnsFromModel(false);
		// tblSearchResult.setPreferredSize(new Dimension(525, 110));

		JScrollPane scrollPane = new JScrollPane(tblSearchResult);
		// scrollPane.setAutoscrolls(true);
		scrollPane.setPreferredSize(new Dimension(750, 150));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		// scrollPane.setViewportView(tblSearchResult);

		pnlSearchResult.add(scrollPane);

		tblSearchResult.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(final java.awt.event.MouseEvent e) {
				displayValuesWithName(aProductList.get(tblSearchResult.getSelectedRow()));
			}
		});

		JTabbedPane tabbedPane1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane1.setBackground(GlobalSettings.PANEL_BLUE);
		tabbedPane1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tabbedPane1.setBounds(812, 393, 532, 212);

		pnlSubCenter.add(tabbedPane1);

		JPanel pnlStockItem = new JPanel();
		tabbedPane1.addTab("Stock item", null, pnlStockItem, null);
		pnlStockItem.setBackground(Color.LIGHT_GRAY);
		// pnlSubCenter.add(pnlStockItem);
		// pnlStockItem.setLayout(null);

		String[] colStockItem = {"WS Code", "WS Prod #", "Prod WS Name", "WS Price", "Disc", "Spes", "idStockitem", "idBonusClass", "idSupplier" };
		tableModStockItem = new DefaultTableModel(colStockItem, 13);
		 tblStockItem = new JTable(tableModStockItem);

/*
		tblStockItem = new JTable(tableModStockItem) {
			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(final TableCellRenderer renderer, final int row, final int column) {
				Component comp = super.prepareRenderer(renderer, row, column);
				// int modelRow = convertRowIndexToModel(row);
				// Object flag = getValueAt(row, 4);
				String flag = (String) getValueAt(row, 5);
				comp.setForeground(Color.BLACK);
				if (flag != null) {
					if (flag.equals("Y")) {
						comp.setForeground(Color.GREEN);
					}
				}
				return comp;
			}
		};
*/
		tblStockItem.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(final java.awt.event.MouseEvent e) {
				int rowIndex = tblStockItem.getSelectedRow();
				//int colIndex = tblStockItem.getSelectedColumn()
				String flag = (String) tblStockItem.getValueAt(rowIndex, 5);
				String prodName = (String) tblStockItem.getValueAt(rowIndex, 2);
				int idStockitem = (Integer) tblStockItem.getValueAt(rowIndex, 6);
				int idBonusClass = (Integer) tblStockItem.getValueAt(rowIndex, 7);
				int idSupplier  = (Integer) tblStockItem.getValueAt(rowIndex, 8);
				if (flag.equals("BO")) {
					new DisplayBonusInfo(new JFrame(), prodName, idStockitem, idBonusClass, datasource);
				}
				if (flag.equals("SP")) {
			new DisplaySpecialInfo(new JFrame(), prodName, idStockitem, idSupplier, datasource);
				}

			}
		});

		tblStockItem.getColumnModel().getColumn(0).setPreferredWidth(70);
		tblStockItem.getColumnModel().getColumn(2).setPreferredWidth(250);
		tblStockItem.getColumnModel().getColumn(4).setPreferredWidth(35);
		tblStockItem.getColumnModel().getColumn(5).setPreferredWidth(34);

		tblStockItem.getColumnModel().getColumn(6).setMinWidth(0);
		tblStockItem.getColumnModel().getColumn(6).setMaxWidth(0);
		tblStockItem.getColumnModel().getColumn(7).setMinWidth(0);
		tblStockItem.getColumnModel().getColumn(7).setMaxWidth(0);
		tblStockItem.getColumnModel().getColumn(8).setMinWidth(0);
		tblStockItem.getColumnModel().getColumn(8).setMaxWidth(0);

		MyColorColumnRenderer corRend = new MyColorColumnRenderer();
		TableColumnModel colModel = tblStockItem.getColumnModel();
		colModel.getColumn(7).setCellRenderer(corRend);

		pnlStockItem.setLayout(new BorderLayout(0, 0));
		// pnlStockItem.add(table);
		JScrollPane scrollPane_2 = new JScrollPane(tblStockItem);
		
	
		tblStockItem.setShowVerticalLines(true);
		
		pnlStockItem.add(scrollPane_2);

		// JPanel pnlStockItem = new JPanel();
		JPanel pnlActiveIng = new JPanel();
		pnlActiveIng.setBackground(GlobalSettings.PANEL_BLUE);
		tabbedPane1.addTab("Active Ingredient", null, pnlActiveIng, null);
		pnlActiveIng.setLayout(null);
		btnNewStrength2.setFont(new Font("Verdana", Font.BOLD, 11));
		btnNewStrength2.setForeground(new Color(255, 255, 255));
		btnNewStrength2.setBackground(GlobalSettings.BUTTON_ORANGE);

		// btnNewStrength.setIcon(new
		// ImageIcon("C:\\CaturaBackEnd\\bin\\add.png"));
		// btnNewStrength.setSelectedIcon(null);
		// btnNewStrength.setBounds(523, 26, 24, 23);

		btnNewStrength2.addActionListener(this);

		// pnlActiveIng.add(btnNewStrength2);

		spnrIngPoition.setModel(new SpinnerNumberModel(1, 1, 15, 1));
		spnrIngPoition.setBounds(10, 26, 41, 29);
		pnlActiveIng.add(spnrIngPoition);

		JLabel lblPosition = new JLabel("Position");
		lblPosition.setForeground(new Color(255, 255, 255));
		lblPosition.setFont(new Font("Verdana", Font.PLAIN, 11));
		lblPosition.setBounds(10, 11, 46, 14);
		pnlActiveIng.add(lblPosition);

		JLabel lblActiveIngredient = new JLabel("Active Ingredient");
		lblActiveIngredient.setForeground(new Color(255, 255, 255));
		lblActiveIngredient.setFont(new Font("Verdana", Font.PLAIN, 11));
		lblActiveIngredient.setBounds(61, 11, 179, 14);
		pnlActiveIng.add(lblActiveIngredient);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 57, 523, 92);
		pnlActiveIng.add(panel_1);

		String[] colActiveIng = { "Position", "Active Ingredient", "Strength", "id" };
		tableModelActiveIng = new DefaultTableModel(colActiveIng, 7);
		// tblSearchResult = new JTable(tableModSearchResult);
		tblActiveIngredient = new JTable(tableModelActiveIng);
		tblActiveIngredient.getColumnModel().getColumn(3).setMinWidth(0);
		tblActiveIngredient.getColumnModel().getColumn(3).setMaxWidth(0);

		JScrollPane scrollPane_1 = new JScrollPane(tblActiveIngredient);
		scrollPane_1.setPreferredSize(new Dimension(500, 150));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));
		panel_1.add(scrollPane_1);

		JTableHeader header = tblActiveIngredient.getTableHeader();
		header.setForeground(Color.BLUE);
		header.setBackground(Color.red);

		tblActiveIngredient.setRowSelectionAllowed(true);
		tblActiveIngredient.setOpaque(false);
		tblActiveIngredient.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// pnlActiveIng.add(btnUpdateStrenth);

		cmbActiveIngredient.setBounds(58, 26, 447, 29);
		AutoCompletion.enable(cmbActiveIngredient);
		pnlActiveIng.add(cmbActiveIngredient);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(GlobalSettings.PANEL_BLUE);
		panel_7.setBounds(10, 149, 495, 31);
		pnlActiveIng.add(panel_7);
				btnAddIngr.setBackground(GlobalSettings.BUTTON_ORANGE);
				btnAddIngr.setForeground(Color.WHITE);
				btnAddIngr.setFont(new Font("Verdana", Font.BOLD, 11));
				panel_7.add(btnAddIngr);
				btnRemoveIngr.setBackground(GlobalSettings.BUTTON_ORANGE);
				btnRemoveIngr.setForeground(Color.WHITE);
				btnRemoveIngr.setFont(new Font("Verdana", Font.BOLD, 11));
				panel_7.add(btnRemoveIngr);
						btnReplaceIngr.setBackground(GlobalSettings.BUTTON_ORANGE);
						btnReplaceIngr.setForeground(Color.WHITE);
						btnReplaceIngr.setFont(new Font("Verdana", Font.BOLD, 11));
						panel_7.add(btnReplaceIngr);
				
						btnReplaceIngr.addActionListener(this);
				btnRemoveIngr.addActionListener(this);
		
				btnAddIngr.addActionListener(this);

		JPanel pnlClassification = new JPanel();
		tabbedPane1.addTab("Classification", null, pnlClassification, null);
		pnlClassification.setLayout(new BorderLayout(0, 0));

		JPanel pnlIngredientList = new JPanel();
		pnlIngredientList.setBackground(GlobalSettings.PANEL_BLUE);
		tabbedPane1.addTab("Ingredient", null, pnlIngredientList, null);
		pnlIngredientList.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(GlobalSettings.PANEL_BLUE);
		panel_2.setBounds(0, 56, 523, 89);
		pnlIngredientList.add(panel_2);

		String[] colIng = { "Ingr", "Strength", "" };
		tableModelActiveIngList = new DefaultTableModel(colIng, 8);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		tblIngredientList = new JTable(tableModelActiveIngList);

		/*
		 * tblIngredientList = new JTable(); tblIngredientList.setModel(new
		 * DefaultTableModel( new Object[][] { {null, null}, {null, null}, {null, null},
		 * {null, null}, {null, null}, {null, null}, {null, null}, {null, null}, }, new
		 * String[] { "Ingr", "Strength" } ));
		 */
		JScrollPane scrollPane2 = new JScrollPane(tblIngredientList);
		panel_2.add(scrollPane2);

		tblIngredientList.getColumnModel().getColumn(0).setPreferredWidth(400);
		tblIngredientList.getColumnModel().getColumn(1).setPreferredWidth(100);
		tblIngredientList.getColumnModel().getColumn(2).setPreferredWidth(0);

		cmbIngredient2.setBounds(0, 21, 219, 31);
		AutoCompletion.enable(cmbIngredient2);
		pnlIngredientList.add(cmbIngredient2);

		cmbStrength2.setBounds(314, 21, 133, 31);
		AutoCompletion.enable(cmbStrength2);
		pnlIngredientList.add(cmbStrength2);

		// JButton btnNewIngredient2 = new JButton("");
		btnNewIngredient2.setIcon(new ImageIcon("C:\\CaturaBackEnd\\icon\\add.png"));
		btnNewIngredient2.setBounds(232, 21, 72, 29);
		pnlIngredientList.add(btnNewIngredient2);

		// JButton btnNewStrength2 = new JButton("");
		btnNewStrength2.setIcon(new ImageIcon("C:\\CaturaBackEnd\\icon\\add.png"));
		btnNewStrength2.setBounds(457, 21, 66, 31);
		pnlIngredientList.add(btnNewStrength2);

		JLabel lblIngredient = new JLabel("Ingredient");
		lblIngredient.setForeground(new Color(255, 255, 255));
		lblIngredient.setFont(new Font("Verdana", Font.PLAIN, 11));
		lblIngredient.setBounds(10, 0, 84, 14);
		pnlIngredientList.add(lblIngredient);

		JLabel lblStrength_1 = new JLabel("Strength");
		lblStrength_1.setForeground(new Color(255, 255, 255));
		lblStrength_1.setFont(new Font("Verdana", Font.PLAIN, 11));
		lblStrength_1.setBounds(320, 0, 65, 14);
		pnlIngredientList.add(lblStrength_1);
		pnlClassification.setLayout(new BorderLayout(0, 0));

		// --------------
		rowSorter = new TableRowSorter<>(tblIngredientList.getModel());

		tblIngredientList.setRowSorter(rowSorter);
								btnCreateActIngr.setBounds(0, 151, 133, 29);
								pnlIngredientList.add(btnCreateActIngr);
								btnCreateActIngr.setBackground(GlobalSettings.BUTTON_ORANGE);
								btnCreateActIngr.setForeground(new Color(255, 255, 255));
								btnCreateActIngr.setFont(new Font("Verdana", Font.BOLD, 11));
								btnUpdateStrenth2.setBounds(374, 151, 141, 29);
								pnlIngredientList.add(btnUpdateStrenth2);
								btnUpdateStrenth2.setBackground(GlobalSettings.BUTTON_ORANGE);
								btnUpdateStrenth2.setForeground(new Color(255, 255, 255));
								btnUpdateStrenth2.setFont(new Font("Verdana", Font.BOLD, 11));
								
										JLabel lblSearch = new JLabel("Search:");
										lblSearch.setBounds(142, 156, 45, 19);
										pnlIngredientList.add(lblSearch);
										lblSearch.setFont(new Font("Verdana", Font.PLAIN, 11));
										lblSearch.setForeground(new Color(255, 255, 255));
										
												edtSearchIngr = new JTextField();
												edtSearchIngr.setBounds(197, 150, 169, 29);
												pnlIngredientList.add(edtSearchIngr);
												edtSearchIngr.setColumns(10);
												
														edtSearchIngr.getDocument().addDocumentListener(new DocumentListener() {
												
															@Override
															public void insertUpdate(DocumentEvent e) {
																String text = edtSearchIngr.getText();
												
																if (text.trim().length() == 0) {
																	rowSorter.setRowFilter(null);
																} else {
																	rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
																}
															}
												
															@Override
															public void removeUpdate(DocumentEvent e) {
																String text = edtSearchIngr.getText();
												
																if (text.trim().length() == 0) {
																	rowSorter.setRowFilter(null);
																} else {
																	rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
																}
															}
												
															@Override
															public void changedUpdate(DocumentEvent e) {
																throw new UnsupportedOperationException("Not supported yet.");
																// To change body of generated methods, choose Tools |
																// Templates.
															}
												
														});
								btnUpdateStrenth2.addActionListener(this);
								
										btnCreateActIngr.addActionListener(this);

		// ------------
		JPanel panel3 = new JPanel();
		panel3.setBackground(GlobalSettings.PANEL_BLUE);
		pnlClassification.add(panel3);
		panel3.setLayout(null);
		AutoCompletion.enable(cmbMimsLevel2);
		AutoCompletion.enable(cmbMimsLevel3);
		panel3.add(btnAddClassification);
		
		panel_36 = new JPanel();
		panel_36.setBackground(GlobalSettings.PANEL_BLUE);
		panel_36.setBounds(10, 11, 495, 77);
		panel3.add(panel_36);
				panel_36.setLayout(new GridLayout(3, 2, -320, 0));
		
				JLabel lblNewLabel_1 = new JLabel("MIMS Level 1 :");
				lblNewLabel_1.setFont(new Font("Verdana", Font.PLAIN, 11));
				lblNewLabel_1.setForeground(new Color(255, 255, 255));
				panel_36.add(lblNewLabel_1);
				AutoCompletion.enable(cmbMimsLevel1);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(812, 249, 532, 133);
		panel_3.setBackground(GlobalSettings.PANEL_BLUE);
		panel_3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_3.setLayout(new BorderLayout(0, 0));

		JPanel panel_4 = new JPanel();
		panel_4.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout = (FlowLayout) panel_4.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel_3.add(panel_4, BorderLayout.NORTH);

		JLabel lblWsStockItem = new JLabel("WS Stock Item");
		lblWsStockItem.setForeground(new Color(255, 255, 255));
		panel_4.add(lblWsStockItem);
		lblWsStockItem.setFont(new Font("Verdana", Font.BOLD, 11));

		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5, BorderLayout.CENTER);

		JPanel panel_19 = new JPanel();
		panel_19.setBackground(GlobalSettings.PANEL_BLUE);

		JPanel panel_6 = new JPanel();
		panel_6.setBackground(GlobalSettings.PANEL_BLUE);
		panel_6.setLayout(new GridLayout(0, 2, -140, 0));

		JLabel lblNewLabel_4 = new JLabel("Product Number:");
		lblNewLabel_4.setForeground(new Color(255, 255, 255));
		panel_6.add(lblNewLabel_4);

		edtStockItemProdNo = new JTextField();
		panel_6.add(edtStockItemProdNo);
		edtStockItemProdNo.setColumns(10);

		JLabel lblNewLabel_5 = new JLabel("Product Description:");
		lblNewLabel_5.setForeground(new Color(255, 255, 255));
		panel_6.add(lblNewLabel_5);

		edtStockItemDesc = new JTextField();
		panel_6.add(edtStockItemDesc);
		edtStockItemDesc.setColumns(10);
		cmbSupplierList.setPreferredSize(new Dimension(28, 30));
		cmbSupplierList.setSelectedIndex(-1);
		
	
		
		btnAddStockItem.setForeground(new Color(255, 255, 255));
		btnAddStockItem.setFont(new Font("Verdana", Font.BOLD, 11));
		btnAddStockItem.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnAddStockItem.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnAddStockItem.addActionListener(this);

		JPanel panel2 = new JPanel();
		panel2.setBackground(GlobalSettings.PANEL_BLUE);
		panel2.setLayout(new GridLayout(0, 1, 0, 0));
		btnReLinkStockItem.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnReLinkStockItem.setForeground(new Color(255, 255, 255));
		btnReLinkStockItem.setFont(new Font("Verdana", Font.BOLD, 11));

		//
		btnReLinkStockItem.setBorder(null);
		btnReLinkStockItem.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnReLinkStockItem.setToolTipText("Link Stockitem to different product");
		btnReLinkStockItem.addActionListener(this);
		panel_5.setLayout(new GridLayout(0, 1, 0, 0));
		panel2.add(btnReLinkStockItem);

		panel2.add(btnAddStockItem);
		GroupLayout gl_panel_19 = new GroupLayout(panel_19);
		gl_panel_19.setHorizontalGroup(gl_panel_19.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_19.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel_19.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_19.createSequentialGroup()
										.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(panel2, GroupLayout.PREFERRED_SIZE, 140,
												GroupLayout.PREFERRED_SIZE)
										.addContainerGap())
								.addGroup(gl_panel_19.createSequentialGroup()
										.addComponent(cmbSupplierList, 0, 358, Short.MAX_VALUE).addGap(156)))));
		gl_panel_19.setVerticalGroup(gl_panel_19.createParallelGroup(Alignment.LEADING).addGroup(gl_panel_19
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel_19.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(panel2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)
						.addComponent(panel_6, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(cmbSupplierList, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(21, Short.MAX_VALUE)));
		panel_19.setLayout(gl_panel_19);
		panel_5.add(panel_19);

		JPanel panel_8 = new JPanel();
		panel_8.setBounds(520, 10, 147, 133);
		panel_8.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_8.setBackground(GlobalSettings.PANEL_BLUE);
		panel_8.setLayout(new GridLayout(4, 0, 2, 2));
		btnIdProdSearch.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnIdProdSearch.setForeground(new Color(255, 255, 255));
		btnIdProdSearch.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_8.add(btnIdProdSearch);

		//
		btnIdProdSearch.setBorder(null);
		btnIdProdSearch.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		// ----------------------------------------------
		JButton btnSearchBarcode = new JButton("Search");
		btnSearchBarcode.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnSearchBarcode.setForeground(new Color(255, 255, 255));
		btnSearchBarcode.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_8.add(btnSearchBarcode);
		btnSearchBarcode.setBorder(null);

		// ----------------------------------------------
		JButton btnSearchNappicode = new JButton("Search");
		btnSearchNappicode.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnSearchNappicode.setForeground(new Color(255, 255, 255));
		btnSearchNappicode.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_8.add(btnSearchNappicode);
		btnSearchNappicode.setBorder(null);
		btnDelProd.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnDelProd.setForeground(new Color(255, 255, 255));
		btnDelProd.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_8.add(btnDelProd);
		//
		btnDelProd.setBorder(null);
		btnDelProd.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnDelProd.setToolTipText("Remove product with no stockitems");

		JPanel panel_9 = new JPanel();
		panel_9.setBounds(812, 10, 532, 144);
		panel_9.setBackground(GlobalSettings.PANEL_BLUE);
		panel_9.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_9.setLayout(new BorderLayout(0, 0));

		JPanel panel_10 = new JPanel();
		panel_10.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_1 = (FlowLayout) panel_10.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panel_9.add(panel_10, BorderLayout.NORTH);

		JLabel lblSearch_1 = new JLabel("Search");
		lblSearch_1.setForeground(new Color(255, 255, 255));
		lblSearch_1.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_10.add(lblSearch_1);

		JPanel panel_11 = new JPanel();
		panel_9.add(panel_11, BorderLayout.CENTER);

		JPanel panel_15 = new JPanel();
		panel_15.setLayout(new GridLayout(3, 0, 0, 0));

		JPanel panel_13 = new JPanel();
		panel_15.add(panel_13);
		panel_13.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel_14 = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) panel_14.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		panel_14.setBackground(GlobalSettings.PANEL_BLUE);
		panel_13.add(panel_14);
		
		edtProductNameSearch1.addKeyListener(this);
		panel_14.add(edtProductNameSearch1);
		edtProductNameSearch1.setColumns(20);

		// search for product
		JButton btnSearchName = new JButton("Search");
		btnSearchName.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnSearchName.setForeground(new Color(255, 255, 255));
		btnSearchName.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_14.add(btnSearchName);
		btnStartWith.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnStartWith.setForeground(new Color(255, 255, 255));
		btnStartWith.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_14.add(btnStartWith);

		JPanel panel_12 = new JPanel();
		panel_12.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_4 = (FlowLayout) panel_12.getLayout();
		flowLayout_4.setAlignment(FlowLayout.LEFT);
		panel_15.add(panel_12);

		JLabel lblAnd1 = new JLabel(AND);
		lblAnd1.setForeground(new Color(255, 255, 255));
		panel_12.add(lblAnd1);
		panel_12.add(edtProductName2);
		edtProductName2.setColumns(10);

		JLabel lblAnd2 = new JLabel(AND);
		lblAnd2.setForeground(new Color(255, 255, 255));
		panel_12.add(lblAnd2);
		panel_12.add(edtProductName3);
		edtProductName3.setColumns(10);

		JLabel lblAnd3 = new JLabel(AND);
		lblAnd3.setForeground(new Color(255, 255, 255));
		panel_12.add(lblAnd3);
		panel_12.add(edtProductName4);
		edtProductName4.setColumns(10);

		JLabel lblAnd4 = new JLabel(AND);
		lblAnd4.setForeground(new Color(255, 255, 255));
		panel_12.add(lblAnd4);
		panel_12.add(edtProductName5);
		edtProductName5.setColumns(10);

		JPanel panel_21 = new JPanel();
		FlowLayout flowLayout_14 = (FlowLayout) panel_21.getLayout();
		flowLayout_14.setAlignment(FlowLayout.LEFT);
		panel_21.setBackground(GlobalSettings.PANEL_BLUE);
		panel_15.add(panel_21);

		// Save Product changes
		JButton btnSaveChanges = new JButton("Save Product Changes");
		btnSaveChanges.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnSaveChanges.setForeground(new Color(255, 255, 255));
		btnSaveChanges.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_21.add(btnSaveChanges);
		btnSaveChanges.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		// insert product detail only
		JButton btnCreateProduct = new JButton("Create Product");
		btnCreateProduct.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnCreateProduct.setForeground(new Color(255, 255, 255));
		btnCreateProduct.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_21.add(btnCreateProduct);

		//
		JButton btnNewButton = new JButton("Create product & Stock");
		btnNewButton.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_21.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				// check if product detail ok
				Product product2 = getProductDetail();
				if (product2 != null) {
					// Check stock item settings
					if (insertStockItem(2)) {
						// product and stockitem ok - insert to db
						sendNewProductToDb(product2);
					}
				}
			}
		});
		btnCreateProduct.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// test if product detail ok
				Product product2 = getProductDetail();
				// only if insert product detail
				if (product2 != null) {
					sendNewProductToDb(product2);
				}
			}
		});
		btnSaveChanges.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// check if product detail ok
				Product product2 = getProductDetail();
				if (product2 != null) {
					// product detail ok update db
					sendProductUpdateToDb(product2);
					updateSearchTableRow(product2);
				}
			}
		});
		btnStartWith.addActionListener(this);
		btnSearchName.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				searchProduct();
			}
		});
		panel_11.setLayout(new GridLayout(0, 1, 0, 0));
		panel_11.add(panel_15);

		JPanel panel_16 = new JPanel();
		panel_16.setBounds(812, 165, 532, 73);
		panel_16.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_16.setBackground(GlobalSettings.PANEL_BLUE);
		panel_16.setLayout(new BorderLayout(0, 0));

		JPanel panel_17 = new JPanel();
		panel_17.setBackground(GlobalSettings.PANEL_BLUE);
		panel_16.add(panel_17, BorderLayout.NORTH);
		panel_17.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JLabel lblMyPasteField = new JLabel("My Paste Field");
		lblMyPasteField.setForeground(new Color(255, 255, 255));
		panel_17.add(lblMyPasteField);
		lblMyPasteField.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMyPasteField.setHorizontalAlignment(SwingConstants.TRAILING);
		panel_17.add(btnClearPastField);
		btnClearPastField.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnClearPastField.setForeground(new Color(255, 255, 255));
		btnClearPastField.setFont(new Font("Verdana", Font.BOLD, 11));
		btnClearPastField.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnClearPastField.addActionListener(this);

		JPanel panel_18 = new JPanel();
		panel_16.add(panel_18, BorderLayout.CENTER);
		panel_18.setLayout(new BorderLayout(0, 0));
		edtMyPasteField.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_18.add(edtMyPasteField);

		edtMyPasteField.setBackground(new Color(255, 255, 255));
		edtMyPasteField.setForeground(Color.BLACK);

		JPanel panel_22 = new JPanel();
		panel_22.setBounds(5, 10, 505, 110);
		panel_22.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_22.setBackground(GlobalSettings.PANEL_BLUE);
		panel_22.setLayout(new GridLayout(4, 2, -250, 0));

		JLabel lblNewLabel_6 = new JLabel("Product ID:");
		lblNewLabel_6.setForeground(new Color(255, 255, 255));
		lblNewLabel_6.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_22.add(lblNewLabel_6);
		panel_22.add(edtProductID);
		edtProductID.setEditable(true);
		edtProductID.setEnabled(true);
		edtProductID.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("Product Barcode:");
		lblNewLabel_7.setForeground(new Color(255, 255, 255));
		lblNewLabel_7.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_22.add(lblNewLabel_7);
		panel_22.add(edtProductBarcode);
		edtProductBarcode.setColumns(10);

		JLabel lblNewLabel_8 = new JLabel("Product NAPPI Code:");
		lblNewLabel_8.setForeground(new Color(255, 255, 255));
		lblNewLabel_8.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_22.add(lblNewLabel_8);
		panel_22.add(edtProductNappicode);
		edtProductNappicode.setColumns(10);

		JLabel lblNewLabel_9 = new JLabel("Product Name:");
		lblNewLabel_9.setForeground(new Color(255, 255, 255));
		lblNewLabel_9.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_22.add(lblNewLabel_9);

		// ----------------------------------------------
		edtproductName = new JTextField();
		panel_22.add(edtproductName);
		edtproductName.setColumns(10);

		JPanel panel_23 = new JPanel();
		panel_23.setBounds(5, 214, 379, 44);
		FlowLayout flowLayout_13 = (FlowLayout) panel_23.getLayout();
		flowLayout_13.setAlignment(FlowLayout.LEFT);
		panel_23.setBackground(GlobalSettings.PANEL_BLUE);
		panel_23.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		JLabel lblProductPack = new JLabel("Pack  Size:");
		lblProductPack.setForeground(new Color(255, 255, 255));
		panel_23.add(lblProductPack);
		lblProductPack.setHorizontalAlignment(SwingConstants.TRAILING);
		panel_23.add(edtProductPack);
		edtProductPack.setColumns(8);

		JLabel lblProductDosageForm = new JLabel("Dosage Form :");
		lblProductDosageForm.setForeground(new Color(255, 255, 255));
		panel_23.add(lblProductDosageForm);
		lblProductDosageForm.setHorizontalAlignment(SwingConstants.TRAILING);
		panel_23.add(edtDosageForm);
		edtDosageForm.setColumns(8);

		JPanel panel_25 = new JPanel();
		panel_25.setBounds(394, 309, 396, 41);
		panel_25.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_25.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_6 = (FlowLayout) panel_25.getLayout();
		flowLayout_6.setAlignment(FlowLayout.LEFT);
		JLabel lblProductSched = new JLabel("Product Schedule :");
		lblProductSched.setForeground(new Color(255, 255, 255));
		lblProductSched.setBackground(new Color(255, 255, 255));
		panel_25.add(lblProductSched);
		lblProductSched.setHorizontalAlignment(SwingConstants.TRAILING);
		cmbProductSched.setPreferredSize(new Dimension(200, 30));
		panel_25.add(cmbProductSched);
		cmbProductSched.setModel(new DefaultComboBoxModel<String>(prodSchedOptions));

		JPanel panel_26 = new JPanel();
		panel_26.setBounds(5, 309, 379, 41);
		panel_26.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_26.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_7 = (FlowLayout) panel_26.getLayout();
		flowLayout_7.setAlignment(FlowLayout.LEFT);

		JLabel lblProductStrength = new JLabel("Product Strength :");
		lblProductStrength.setForeground(new Color(255, 255, 255));
		lblProductStrength.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_26.add(lblProductStrength);
		lblProductStrength.setHorizontalAlignment(SwingConstants.TRAILING);
		panel_26.add(edtProductStrength);
		edtProductStrength.setColumns(20);

		JPanel panel_27 = new JPanel();
		panel_27.setBounds(394, 163, 396, 44);
		panel_27.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_27.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_8 = (FlowLayout) panel_27.getLayout();
		flowLayout_8.setAlignment(FlowLayout.LEFT);

		JLabel lblProductType = new JLabel("Product Type :");
		lblProductType.setForeground(new Color(255, 255, 255));
		lblProductType.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_27.add(lblProductType);
		lblProductType.setHorizontalAlignment(SwingConstants.TRAILING);
		cmbProductType.setPreferredSize(new Dimension(290, 30));
		panel_27.add(cmbProductType);

		cmbProductType.setModel(new DefaultComboBoxModel(
				new String[] { "E = Ethical Products", "EO = Ethical Original", "EG = Ethical Generic",
						"EC = Ethical Clone", "S = Surgical Products", "V = Veterinary Products", "O = Other", "" }));

		JPanel panel_28 = new JPanel();
		panel_28.setBounds(394, 214, 396, 39);
		panel_28.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_28.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_10 = (FlowLayout) panel_28.getLayout();
		flowLayout_10.setAlignment(FlowLayout.LEFT);

		JLabel lblProductCataloguenumber = new JLabel("Product Catalogue Number:");
		lblProductCataloguenumber.setForeground(new Color(255, 255, 255));
		lblProductCataloguenumber.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_28.add(lblProductCataloguenumber);
		lblProductCataloguenumber.setHorizontalAlignment(SwingConstants.TRAILING);
		panel_28.add(edtCatalogueNum);
		edtCatalogueNum.setColumns(18);

		JPanel panel_29 = new JPanel();
		panel_29.setBounds(394, 259, 396, 44);
		panel_29.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_29.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_11 = (FlowLayout) panel_29.getLayout();
		flowLayout_11.setAlignment(FlowLayout.LEFT);

		JLabel lblManufacturerName = new JLabel("Manufacturer Name:");
		lblManufacturerName.setForeground(new Color(255, 255, 255));
		lblManufacturerName.setFont(new Font("Verdana", Font.PLAIN, 11));
		panel_29.add(lblManufacturerName);
		lblManufacturerName.setHorizontalAlignment(SwingConstants.TRAILING);
		cmbManufacturer.setPreferredSize(new Dimension(180, 30));
		panel_29.add(cmbManufacturer);
		cmbManufacturer.setSelectedIndex(-1);

		// set manufacturer to none
		JButton btnNoneManuf = new JButton("none");
		btnNoneManuf.setForeground(new Color(255, 255, 255));
		btnNoneManuf.setBackground(GlobalSettings.BUTTON_ORANGE);
		panel_29.add(btnNoneManuf);

		btnNoneManuf.setFont(new Font("Verdana", Font.BOLD, 12));

		JPanel panel_30 = new JPanel();
		panel_30.setBounds(5, 126, 379, 82);
		panel_30.setBackground(GlobalSettings.PANEL_BLUE);
		panel_30.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_30.setLayout(new BorderLayout(0, 0));

		JPanel panel_31 = new JPanel();
		panel_31.setBackground(GlobalSettings.PANEL_BLUE);
		FlowLayout flowLayout_9 = (FlowLayout) panel_31.getLayout();
		flowLayout_9.setHgap(0);
		flowLayout_9.setAlignment(FlowLayout.LEFT);
		panel_30.add(panel_31, BorderLayout.NORTH);

		JLabel lblProductDescription = new JLabel("Product Description :");
		panel_31.add(lblProductDescription);
		lblProductDescription.setForeground(new Color(255, 255, 255));
		lblProductDescription.setFont(new Font("Verdana", Font.BOLD, 11));
		panel_30.add(edtProductDescrip, BorderLayout.CENTER);
		edtProductDescrip.setBorder(null);
		edtProductDescrip.setLineWrap(true);
		edtProductDescrip.setWrapStyleWord(true);
		edtProductDescrip.setDocument(new PlainDocument() {
			private static final long serialVersionUID = 1L;

			@Override
			public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
				if (str == null || edtProductDescrip.getText().length() >= MAX_LENGTH) {
					return;
				}

				super.insertString(offs, str, a);
			}
		});
		
		


		JPanel panel_32 = new JPanel();
		panel_32.setBounds(5, 264, 379, 41);
		FlowLayout flowLayout_5 = (FlowLayout) panel_32.getLayout();
		flowLayout_5.setAlignment(FlowLayout.LEFT);
		panel_32.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_32.setBackground(GlobalSettings.PANEL_BLUE);

		JLabel lblProductForm = new JLabel("Product Form :");
		panel_32.add(lblProductForm);
		lblProductForm.setForeground(new Color(255, 255, 255));
		lblProductForm.setHorizontalAlignment(SwingConstants.TRAILING);

		// cmbProductForm = new JComboBox<String>();
		cmbProductForm = new JComboBox<String>();
		panel_32.add(cmbProductForm);
		cmbProductForm.setPreferredSize(new Dimension(200, 30));
		cmbProductForm.setSelectedIndex(-1);

		// set product form to none
		JButton btnNoneProdForm = new JButton("none");
		panel_32.add(btnNoneProdForm);
		btnNoneProdForm.setForeground(new Color(255, 255, 255));
		btnNoneProdForm.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnNoneProdForm.setFont(new Font("Verdana", Font.BOLD, 12));
		pnlSubCenter.setLayout(null);
		pnlSubCenter.add(panel_23);
		pnlSubCenter.add(panel_27);
		pnlSubCenter.add(panel_32);
		pnlSubCenter.add(panel_28);
		pnlSubCenter.add(panel_25);
		pnlSubCenter.add(panel_29);
		pnlSubCenter.add(panel_26);
		pnlSubCenter.add(panel_30);
		pnlSubCenter.add(panel_22);
		pnlSubCenter.add(panel_8);
		pnlSubCenter.add(pnlSearchResult);
		pnlSubCenter.add(panel_9);
		pnlSubCenter.add(panel_16);
		pnlSubCenter.add(panel_3);
		pnlSubCenter.add(tabbedPane1);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.WHITE);
		separator.setForeground(Color.BLACK);

		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(800, 0, 10, 627);
		pnlSubCenter.add(separator);

		JPanel panel_35 = new JPanel();
		panel.add(panel_35, BorderLayout.SOUTH);
		panel_35.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel_20 = new JPanel();
		panel_35.add(panel_20);
		FlowLayout flowLayout_12 = (FlowLayout) panel_20.getLayout();
		flowLayout_12.setAlignment(FlowLayout.LEFT);
		flowLayout_12.setVgap(2);
		panel_20.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_20.setBackground(GlobalSettings.PANEL_BLUE);

		// ----------------------------------------------
		JButton btnReset = new JButton("Reset");
		btnReset.setFont(new Font("Verdana", Font.BOLD, 12));
		btnReset.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnReset.setForeground(new Color(255, 255, 255));
		panel_20.add(btnReset);

		// ----------------------------------------------

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setFont(new Font("Verdana", Font.BOLD, 12));
		btnCancel.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnCancel.setForeground(new Color(255, 255, 255));
		panel_20.add(btnCancel);

		JLabel lblDisplayName = new JLabel("Display Name :");
		lblDisplayName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDisplayName.setForeground(new Color(255, 255, 255));
		panel_20.add(lblDisplayName);
		panel_20.add(lblNewDisplayName);

		lblNewDisplayName.setForeground(new Color(0, 0, 0));
		lblNewDisplayName.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				pnlProduct.dispose();
				Menu menu = new Menu();
				menu.pnlMenu.setVisible(true);
			}
		});
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				cleanTextField();
			}
		});

		btnNoneProdForm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				cmbProductForm.setSelectedIndex(0);
			}
		});
		btnNoneManuf.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				cmbManufacturer.setSelectedIndex(0);
			}
		});
		btnDelProd.addActionListener(this);
		btnSearchNappicode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				tableModSearchResult.setRowCount(0);
				nappicode = edtProductNappicode.getText();
				product = productdao.getProductOnNappi(datasource, nappicode);

				if (product != null) {
					displayValuesWithNappicode(product); // 530671003
				} else {
					JOptionPane.showMessageDialog(null, "Nothin found.");
				}
			}
		});
		btnSearchBarcode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				tableModSearchResult.setRowCount(0);
				barcode = edtProductBarcode.getText();
				product = productdao.getProductOnBarcode(datasource, barcode);
				if (product != null) {
					displayValuesWithBarcode(product); // 6001137100179
				} else {
					JOptionPane.showMessageDialog(null, "Nothin found.");
				}
			}
		});
		btnIdProdSearch.addActionListener(this);
		tblActiveIngredient.getColumnModel().getColumn(0).setPreferredWidth(20);
		tblActiveIngredient.getColumnModel().getColumn(1).setPreferredWidth(300);

		// panel_1.add(table);
		getListOfProductForms();
		getListOfIngredient();
		getListOfStrength();
		getListMimsLevel1();
		getListMimsLevel2();
		getListMimsLevel3();
		buildIngredientListTable();
		getListOfActiveIngredient();
		
		
		

	}

	// ----------------------------------------------------
	// button listeners
	public final void actionPerformed(final ActionEvent e) {
		// delete product from T.Productdetail
		if (e.getSource() == btnDelProd) {
			deleteProduct();
		}
		// insert stockitem and link o product
		if (e.getSource() == btnAddStockItem) {
			insertStockItem(1);
		}
		//
		
		/////////////////////////////////
		//Relink Stock Item button action
		/////////////////////////////////
		if (e.getSource() == btnReLinkStockItem) {
			int rowIndex = tblStockItem.getSelectedRow();
			if (rowIndex < 0) {
				String message = "Stockitem for linking not selected";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			} else {
				reLinkStockItem();
			}
		}
		//
		if (e.getSource() == btnIdProdSearch) {
			searchProdOnId();
		}
		//
		if (e.getSource() == btnClearPastField) {
			edtMyPasteField.selectAll();
			edtMyPasteField.setText(null);
		}
		//
		if (e.getSource() == btnNewIngredient2) {
			NewIngredient newingredient = new NewIngredient(pnlProduct, datasource);
			if (newingredient.isInserted()) {
				getListOfIngredient();
			}
		}
		if (e.getSource() == btnNewStrength2) {
			NewStrength newstrength = new NewStrength(pnlProduct, datasource);
			if (newstrength.isInserted()) {
				getListOfStrength();
			}
		}

		//
		if (e.getSource() == btnAddIngr) {
			linkIngToProduct();
		}
		//
		if (e.getSource() == btnRemoveIngr) {
			removeActiveIngredient();
			int idProductDetail = numericutil.str2Int(edtProductID.getText());
			Product product2 = productdao.getProductOnId(idProductDetail, datasource);
			buildIngredientTable(product2);
		}
		//
		if (e.getSource() == btnAddClassification) {
			addClassification();
		}

		//
		if (e.getSource() == btnUpdateStrenth2) {
			updateStrenth();
		}
		//
		if (e.getSource() == btnStartWith) {
			getProductBeginWIth();
		}
		if (e.getSource() == btnCreateActIngr) {
			linkNewIngredient();
		}
		if (e.getSource() == btnReplaceIngr) {
			replaceIngr();
		}

	}

	// ------------------------------ private List<ProductHasActiveIngredients>
	// aProductHasIng
	private void replaceIngr() {
		String message1 = "Ingredient replaced";
		String message2 = "Error in replacing Ingredient";

		// get ingr from combo
		int pos = cmbActiveIngredient.getSelectedIndex();
		ActiveIngredients activeingredientsNew = aActiveIngredientsList.get(pos);

		int idProductDetail = numericutil.str2Int(edtProductID.getText());

		// get item to replace from selected table
		int rowIndex = tblActiveIngredient.getSelectedRow();
		producthasactiveingredients = aProductHasIng.get(rowIndex);
		int seqNo = producthasactiveingredients.getIngSeq();

		int count = producthasactiveingredientsdao.updateProductHasActiveIngredients(
				activeingredientsNew.getIdActiveIngredients(), idProductDetail,
				producthasactiveingredients.getIdActiveIngredients(), seqNo, datasource);

		if (count > 0) {
			Product product2 = productdao.getProductOnId(idProductDetail, datasource);
			buildIngredientTable(product2);
			// JOptionPane.showMessageDialog(new JFrame(), message1, "Dialog",
			// JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(new JFrame(), message2, "Dialog", JOptionPane.ERROR_MESSAGE);
		}

	}

	// ----------------------------------
	private void buildIngredientListTable() {
		// active ingredient
		tableModelActiveIngList.setRowCount(0);

		aActiveIngredientsList = activeingredientsdao.getActiveIngredientsList(datasource);

		for (int i = 0; i < aActiveIngredientsList.size(); i++) {
			activeingredients = aActiveIngredientsList.get(i);

			Object[] obj = new Object[3];

			obj[0] = activeingredients.getIngredientName();
			obj[1] = activeingredients.getStrength();
			obj[2] = activeingredients.getIdActiveIngredients();

			tableModelActiveIngList.addRow(obj);

		} // for
	}

	// ------------------------------
	private void updateStrenth() {
		int rowIndex = tblIngredientList.getSelectedRow();
		String message1 = "Strength updated";
		String message2 = "Error in updating strength!!!!!!";
		// get row number
		if (rowIndex >= 0) {
			int id = (Integer) tblIngredientList.getValueAt(rowIndex, 2);
			// log.info("---->>>>"+ tableModelActiveIng.getValueAt(rowIndex,
			// 3));
			// get combo index
			int strengthSelected = cmbStrength2.getSelectedIndex();
			ingredientstrength = aIngredientStrengthList.get(strengthSelected);

			// activeingredients
			int count = activeingredientsdao.updateStrength(id, ingredientstrength.getIdIngredientStrength(),
					datasource);
			if (count > 0) {
				/*
				 * int idProductDetail = numericutil.str2Int(edtProductID.getText()); if
				 * (idProductDetail > 0) { Product product2 =
				 * productdao.getProductOnId(idProductDetail, datasource); // selected product
				 * exist - buildIngredientTable(product2); }
				 */
				buildIngredientListTable();
				JOptionPane.showMessageDialog(new JFrame(), message1, "Dialog", JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(new JFrame(), message2, "Dialog", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	// ---------------------------------
	private void addClassification() {
		String message1 = "Classification updated";
		String message2 = "Error Classification NOT updated";

		mimslevel1 = aListMimsLevel1.get(cmbMimsLevel1.getSelectedIndex());
		mimslevel2 = aListMimsLevel2.get(cmbMimsLevel2.getSelectedIndex());
		mimslevel3 = aListMimsLevel3.get(cmbMimsLevel3.getSelectedIndex());

		int idProductDetail = numericutil.str2Int(edtProductID.getText());

		boolean isUpdated = productdao.updateClassification(idProductDetail, mimslevel1.getIdMimsLevel1(),
				mimslevel2.getIdMimsLevel2(), mimslevel3.getIdMimsLevel3(), datasource);

		if (isUpdated) {
			JOptionPane.showMessageDialog(new JFrame(), message1, "Dialog", JOptionPane.ERROR_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(new JFrame(), message2, "Dialog", JOptionPane.ERROR_MESSAGE);
		}
	}

	// -------------------------------
	private void reLinkStockItem() {
		ClientResource requestResource = null;
		Product product2 = new Product();

		int rowIndex = tblStockItem.getSelectedRow();
		// int colIndex = table.getSelectedColumn();

		StockItem stockitem3 = aStkList.get(rowIndex - 1);

		String id = JOptionPane.showInputDialog(this, "Enter the Product ID to link to:");
		int prodId = numericutil.str2Int(id);
		if (prodId > 0) {
			// get product for id supplied
			try {
				String http = SERVER_HTTP + "/products/getid/idproduct/" + prodId;

				requestResource = new ClientResource(http);
				Representation reply = requestResource.get();

				int returnStatus = requestResource.getStatus().getCode();
				if (returnStatus == 200) {
					// System.out.println(">>>>>>"+requestResource.getText());
					requestResource.release();
					SaxRepresentation sax = new SaxRepresentation(reply);
					// xml to Product object
					// String replyText = reply.getText();
					// System.out.println("Reply Text:" + replyText);

					List<Product> aProdList = productfromxml.getProductData(sax);
					if (aProdList.size() > 0) {
						product2 = aProdList.get(0);
						// display info
						String wsCode = getSupplierCode(stockitem3.getIdSupplier());
						// test if product does not exist for supplier
						if (testProductWsInStockItem(prodId, stockitem3.getIdSupplier())) {
							DisplayProductInfo displayinfo = new DisplayProductInfo(pnlProduct, product2, stockitem3,
									wsCode);
							if (displayinfo.getDoLink()) {
								System.out.println(">>>>do the link>>");
								// do the link
								doStockItemRelink(stockitem3.getIdStockItem(), prodId);
							}
						}
					}
				}
			} catch (ResourceException e) {
				String message = "Product not found on entered ID";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// ------------------------------------
	private boolean testProductWsInStockItem(final int pIdproduct, final int pIdsupplier) {
		ClientResource requestResource = null;
		boolean isRelinkOk = true;

		try {
			String http = SERVER_HTTP + "/stockitem/productforws/idproduct/" + pIdproduct + "/idsupplier/"
					+ pIdsupplier;

			requestResource = new ClientResource(http);
			Representation reply = requestResource.get();

			int returnStatus = requestResource.getStatus().getCode();
			// found on DB can not do link
			if (returnStatus == 200) {
				// System.out.println(">>>>>>"+requestResource.getText());
				requestResource.release();
				isRelinkOk = false;
			}
		} catch (ResourceException e) {
			// not found on DB do relink
			isRelinkOk = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!isRelinkOk) {
			String message = "Cannot link, product exist for wholesaler in stockitem file";
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
		}

		return isRelinkOk;
	}

	// ------------------------------------------
	private void doStockItemRelink(final int pIdstockitem, final int pIdproduct) {
		ClientResource requestResource = null;
		boolean isRelinkOk = true;

		try {
			String http = SERVER_HTTP + "/stockitem/relink/idstockitem/" + pIdstockitem + "/idproduct/" + pIdproduct;

			requestResource = new ClientResource(http);
			Representation reply = requestResource.put(null);

			int returnStatus = requestResource.getStatus().getCode();
			if (returnStatus == 200) {
				// System.out.println(">>>>>>"+requestResource.getText());
				requestResource.release();
				isRelinkOk = true;
			}
		} catch (ResourceException e) {
			isRelinkOk = false;
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (isRelinkOk) {
			String message = "Re-link successfull !!!!!";
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.INFORMATION_MESSAGE);
			searchProduct();
		} else {
			String message = "Re-Link error";
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
		}

	}

	// ----------------------------
	private void deleteProduct() {
		boolean canDelete = true;
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int idProductDetail = numericutil.str2Int(edtProductID.getText());
		ClientResource requestResource = null;

		int dialogResult = JOptionPane.showConfirmDialog(null, "Delete product from Product Master?", "Warning",
				dialogButton);

		if (dialogResult == JOptionPane.YES_OPTION) {
			// check local supplier list for linked stock item
			if (tableModStockItem.getRowCount() > 1) {
				canDelete = false;
			}

			if (canDelete) {
				// check T.Stockitem if idproduct exist
				List<StockItem> aStkList = stockitemdao.getStockItemForProdList(idProductDetail, datasource);
				if (aStkList.size() > 0) {
					// idproduct used in stockfile
					canDelete = false;
				}
			}

			if (canDelete) {
				// remove from T.Productdetail
				try {
					String http = SERVER_HTTP + "/products/delete/idproduct/" + idProductDetail;

					requestResource = new ClientResource(http);
					Representation reply = requestResource.delete();

					int returnStatus = requestResource.getStatus().getCode();
					if (returnStatus == 202) {
						// System.out.println(">>>>>>"+requestResource.getText());
						requestResource.release();
						canDelete = true;
					}
				} catch (ResourceException e) {
					// int returnStatus = requestResource.getStatus().getCode();
					// if (returnStatus == 404) {
					// e.printStackTrace();
					canDelete = false;
				} catch (Exception e) {
					e.printStackTrace();
				}
			} // if

			if (canDelete) {
				String message = "Product removed successfully !!!!!";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.INFORMATION_MESSAGE);
				searchProduct();
			} else {
				String message = "Product can not be removed, product linked to wholesaler stockitem";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	// -----------------------------------
	private void searchProduct() {
		/* Destroying Grid list for new search */
		tableModSearchResult.setRowCount(0);

		name1 = edtProductNameSearch1.getText().toUpperCase();
		name2 = edtProductName2.getText().toUpperCase();
		name3 = edtProductName3.getText().toUpperCase();
		name4 = edtProductName4.getText().toUpperCase();
		name5 = edtProductName5.getText().toUpperCase();

		if (name1.equals("") && name2.equals("") && name3.equals("") && name4.equals("") && name5.equals("")) {
			JOptionPane.showMessageDialog(null, "Nothin found.");
		} else {
			tableModStockItem.setRowCount(1);
			pnlProduct.setCursor(busyCursor);

			aProductList = productdao.getProductNameLike(name1, name2, name3, name4, name5, datasource);
			// aProductList = productdao.getProductNameLike(name1,datasource);
			pnlProduct.setCursor(defaultCursor);

			if (aProductList.size() > 0) {
				pnlProduct.setCursor(busyCursor);
				displayValuesWithName(aProductList.get(0));

				for (int i = 0; (i < 1000 && i < aProductList.size()); ++i) {
					manufacturer = manufacturerdao.getManufacturerOnId(aProductList.get(i).getIdManufacturer(),
							datasource);
					String name = "";
					if (manufacturer != null) {
						name = manufacturer.getManufacturerName();
					}

					List<StockItem> aList = stockitemdao.getStockItemForProdList(aProductList.get(i).getIdProduct(),
							datasource);

					Object[] objects = new Object[8];
					objects[0] = aProductList.get(i).getProductName();
					objects[1] = aProductList.get(i).getProductPack();
					objects[2] = aProductList.get(i).getDosageForm();
					objects[3] = aProductList.get(i).getProductStrength();
					objects[4] = name; // aProductList.get(i).getIdManufacturer();
					objects[5] = aProductList.get(i).getProductNappiCode();
					objects[6] = aProductList.get(i).getProductBarCode();
					objects[7] = aList.size();

					tableModSearchResult.addRow(objects);
				}
				pnlProduct.setCursor(defaultCursor);
			} else {
				JOptionPane.showMessageDialog(null, "Nothin found.");
			}
		}
	}

	// -----------------------------------
	private void getProductBeginWIth() {
		/* Destroying Grid list for new search */
		tableModSearchResult.setRowCount(0);

		name1 = edtProductNameSearch1.getText().toUpperCase();

		if (name1.equals("")) {
			JOptionPane.showMessageDialog(null, "Nothin found.");
		} else {
			tableModStockItem.setRowCount(1);
			pnlProduct.setCursor(busyCursor);

			aProductList = productdao.getProductBeginWith(name1, datasource);

			pnlProduct.setCursor(defaultCursor);

			if (aProductList.size() > 0) {
				pnlProduct.setCursor(busyCursor);
				displayValuesWithName(aProductList.get(0));

				for (int i = 0; (i < 1000 && i < aProductList.size()); ++i) {
					manufacturer = manufacturerdao.getManufacturerOnId(aProductList.get(i).getIdManufacturer(),
							datasource);
					String name = "";
					if (manufacturer != null) {
						name = manufacturer.getManufacturerName();
					}

					List<StockItem> aList = stockitemdao.getStockItemForProdList(aProductList.get(i).getIdProduct(),
							datasource);

					Object[] objects = new Object[8];
					objects[0] = aProductList.get(i).getProductName();
					objects[1] = aProductList.get(i).getProductPack();
					objects[2] = aProductList.get(i).getDosageForm();
					objects[3] = aProductList.get(i).getProductStrength();
					objects[4] = name; // aProductList.get(i).getIdManufacturer();
					objects[5] = aProductList.get(i).getProductNappiCode();
					objects[6] = aProductList.get(i).getProductBarCode();
					objects[7] = aList.size();

					tableModSearchResult.addRow(objects);
				}
				pnlProduct.setCursor(defaultCursor);
			} else {
				JOptionPane.showMessageDialog(null, "Nothin found.");
			}
		}
	}

	// --------------------------------------
	/* Help function to display details of product with barcode */
	private void displayValuesWithBarcode(final Product pProduct) {
		edtProductNameSearch1.setText(pProduct.getProductName());
		// edtProductBarcode.setText(product.getProductBarCode());
		edtProductNappicode.setText(pProduct.getProductNappiCode());
		displayValues(pProduct);
	}

	// --------------------------------------
	/* Display product detail */
	private void displayValuesWithName(final Product pProduct) {
		// edtProductName1.setText(product.getProductName());
		String newName = "";
		String productName = "";
		String flavourColour = "";
		String strength = "";
		String extra = "";
		String dosageForm = "";
		String packZize = "";

		edtProductBarcode.setText(pProduct.getProductBarCode());
		edtProductNappicode.setText(pProduct.getProductNappiCode());
		edtproductName.setText(pProduct.getProductName());
		displayValues(pProduct);
		displayStockItem(pProduct.getIdProduct());

		productName = pProduct.getProductName();
		newName = productName;
		lblNewDisplayName.setText(newName);

	}

	// ------------------------------------
	// get stockitem for product and display product for each ws
	private void displayStockItem(final int pIdProduct) {
		String code = "";
		String isBonusItem = "";
		int idBonusClass = 0;
		List<StockItemBonus> aListBonus = null;
		aStkList = stockitemdao.getStockItemForProdList(pIdProduct, datasource);
		tableModStockItem.setRowCount(1);

		for (int i = 0; i < aStkList.size(); i++) {
			idBonusClass = 0;
			isBonusItem = "";

			stockitem = aStkList.get(i);
			code = getSupplierCode(stockitem.getIdSupplier());
			// get bonus class
			bonusclass = bonusclassdao.getBonusClassOnSupplier(stockitem.getIdSupplier(), datasource);

			// does the product have a special
			if (bonusclass != null) {
				aListBonus = stockitembonusdao.getProductSuppBonus(pIdProduct, stockitem.getIdSupplier(),
						bonusclass.getIdBonusClass(), datasource);
				if (aListBonus.size() > 0) {
					isBonusItem = "BO";
					idBonusClass = bonusclass.getIdBonusClass();
				}
			} else {
				// test for special price
				if (stockitempricedao.isStockItemPriceForSupplier(stockitem.getIdSupplier(), datasource)) {
					isBonusItem = "SP";
				}
			}

			tableModStockItem.addRow(new Object[] { code, stockitem.getStockItemProdNo(), stockitem.getStockItemName(),
					stockitem.getStockItemSellingPrice(), stockitem.getDiscontinued(), isBonusItem,
					stockitem.getIdStockItem(), idBonusClass, stockitem.getIdSupplier() });
		}
	}

	// --------------------------------------
	/* Help function to display details of product with nappicode */
	private void displayValuesWithNappicode(final Product pProduct) {
		edtProductNameSearch1.setText(pProduct.getProductName());
		edtProductBarcode.setText(pProduct.getProductBarCode());
		// edtProductNappicode.setText(product.getProductNappiCode());
		displayValues(pProduct);
	}

	// --------------------------------------
	/*
	 * Help function to display details of product with name using Product parameter
	 */
	private void displayValues(final Product pProduct) {
		Product product2 = productdao.getProductOnId(pProduct.getIdProduct(), datasource);

		edtProductID.setText(String.valueOf(product2.getIdProduct()));
		edtCatalogueNum.setText(product2.getCatalogueNumber());
		edtDosageForm.setText(product2.getDosageForm());
		edtProductStrength.setText(product2.getProductStrength());
		edtProductDescrip.setText(product2.getProductDescription());
		edtProductPack.setText(product2.getProductPack());
		cmbProductSched.setSelectedItem(product2.getProductSchedule());
		cmbProductType.setSelectedItem(displayProductTypeMeaning(product2.getProductType()));
		// edtManufacName.setText(getManufacName(pProduct));
		int pos = Arrays.binarySearch(arManufacturer, getManufacName(product2));
		if (pos >= 0) {
			cmbManufacturer.setSelectedIndex(pos);
		}

		// productform
		for (int i = 0; i < aProductFormList.size(); i++) {
			productform = aProductFormList.get(i);
			if (product2.getIdProductForm() == productform.getIdProductForm()) {
				cmbProductForm.setSelectedIndex(i);
			}
		}

		buildIngredientTable(product2);

		// manufacturer
		int posManufact = Arrays.binarySearch(arManufacturer, getManufacName(product2));
		if (posManufact >= 0) {
			cmbManufacturer.setSelectedIndex(posManufact);
		}

		//
		setMimsLevel(product2);
	}

	// ------------------------------------
	private void updateSearchTableRow(final Product pProduct) {
		int selectedRowIndex = tblSearchResult.getSelectedRow();

		Product product2 = productdao.getProductOnId(pProduct.getIdProduct(), datasource);

		if (selectedRowIndex >= 0) {
			tableModSearchResult.setValueAt(product2.getProductName(), selectedRowIndex, 0);
			tableModSearchResult.setValueAt(product2.getProductPack(), selectedRowIndex, 1);
			tableModSearchResult.setValueAt(product2.getDosageForm(), selectedRowIndex, 2);
			tableModSearchResult.setValueAt(product2.getProductStrength(), selectedRowIndex, 3);
			// tableModSearchResult.setValueAt(NewID, selectedRowIndex, 4);
			tableModSearchResult.setValueAt(product2.getProductNappiCode(), selectedRowIndex, 5);
			tableModSearchResult.setValueAt(product2.getProductBarCode(), selectedRowIndex, 6);
		}
	}

	// ---------------------------------
	private void setMimsLevel(final Product pProduct) {
		//
		int idProductDetail = numericutil.str2Int(edtProductID.getText());

		// Product product2 = productdao.getProductOnId(idProductDetail,
		// datasource);

		for (int i = 0; i < aListMimsLevel1.size(); i++) {
			mimslevel1 = aListMimsLevel1.get(i);
			if (pProduct.getIdMimsLevel1() == mimslevel1.getIdMimsLevel1()) {
				cmbMimsLevel1.setSelectedIndex(i);
				break;
			}
		}
		for (int i = 0; i < aListMimsLevel2.size(); i++) {
			mimslevel2 = aListMimsLevel2.get(i);
			if (pProduct.getIdMimsLevel2() == mimslevel2.getIdMimsLevel2()) {
				cmbMimsLevel2.setSelectedIndex(i);
				break;
			}
		}
		for (int i = 0; i < aListMimsLevel3.size(); i++) {
			mimslevel3 = aListMimsLevel3.get(i);
			if (pProduct.getIdMimsLevel3() == mimslevel3.getIdMimsLevel3()) {
				cmbMimsLevel3.setSelectedIndex(i);
				break;
			}
		}
	}

	// --------------------------------------------------
	/* Help function to RETURN manufacturer name on Product object */
	private String getManufacName(final Product pProduct) {
		String str = "$NONE";
		if (pProduct.getIdManufacturer() != 0) {
			manufacturer = manufacturerdao.getManufacturerOnId(pProduct.getIdManufacturer(), datasource);
			str = manufacturer.getManufacturerName();
		}

		return str;
	}

	/*
	 * Help function to DISPLAY product meaning from vale of product type in
	 * database
	 */
	private String displayProductTypeMeaning(final String pStr) {
		String str = "";
		if (pStr.equals("E")) {
			str = productTypeOptionsMeaning[0];
		}
		if (pStr.equals("EO")) {
			str = productTypeOptionsMeaning[1];
		}
		if (pStr.equals("EG")) {
			str = productTypeOptionsMeaning[2];
		}
		if (pStr.equals("EC")) {
			str = productTypeOptionsMeaning[3];
		}

		if (pStr.equals("S")) {
			str = productTypeOptionsMeaning[4];
		}
		if (pStr.equals("V")) {
			str = productTypeOptionsMeaning[5];
		}
		if (pStr.equals("O")) {
			str = productTypeOptionsMeaning[6];
		}

		return str;
	}

	/* Help function to CLEAN all textfields */
	private void cleanTextField() {
		tableModSearchResult.setRowCount(0);
		edtProductID.setText("");
		edtProductNameSearch1.setText("");
		edtproductName.setText("");
		edtProductName2.setText("");
		edtProductName3.setText("");
		edtProductName4.setText("");
		edtProductName5.setText("");
		edtProductBarcode.setText("");
		edtProductNappicode.setText("");
		edtCatalogueNum.setText("");
		edtDosageForm.setText("");
		edtProductStrength.setText("");
		edtProductDescrip.setText("");
		edtProductPack.setText("");
		cmbProductSched.setSelectedIndex(0);
		cmbProductType.setSelectedItem("");
		cmbManufacturer.setSelectedIndex(-1);
		cmbProductForm.setSelectedIndex(-1);
		cmbProductSched.setSelectedIndex(-1);
		cmbProductType.setSelectedIndex(-1);
		tableModStockItem.setRowCount(1);
		edtStockItemProdNo.setText("");
		edtStockItemDesc.setText("");
		cmbSupplierList.setSelectedIndex(-1);
	}

	/* Help function to CLEAN all textfields except on Grid */
	private void cleanTextFieldExceptGrid() {
		edtProductID.setText("");
		// edtProductName1.setText("");
		// edtProductName1.setText("");
		// edtProductName2.setText("");
		// edtProductName3.setText("");
		// edtProductName4.setText("");
		// edtProductName5.setText("");
		edtProductBarcode.setText("");
		edtProductNappicode.setText("");
		edtCatalogueNum.setText("");
		edtDosageForm.setText("");
		edtProductStrength.setText("");
		edtProductDescrip.setText("");
		edtProductPack.setText("");
		cmbProductSched.setSelectedIndex(0);
		cmbProductType.setSelectedItem("");
		// edtManufacName.setText("");
	}

	// ----------------------------arProductForm
	private void getListOfAllSuppliers() {
		aSuppList = supplierdao.getSuppliersList(datasource);
		arSupplierCode = new String[aSuppList.size()];

		for (int i = 0; i < aSuppList.size(); i++) {
			supplier = aSuppList.get(i);
			arSupplierCode[i] = supplier.getSupplierShortName();
		}
	}

	// ----------------------------
	private void getListOActiveIngr() {
		aActiveIngredientsList = activeingredientsdao.getActiveIngredientsList(datasource);
		arrActiveIng = new String[aActiveIngredientsList.size()];

		for (int i = 0; i < aActiveIngredientsList.size(); i++) {
			activeingredients = aActiveIngredientsList.get(i);
			arrActiveIng[i] = activeingredients.getIngredients();
		}

	}

	// ---------------------------cmbIngredient
	private void getListOfIngredient() {
		if (cmbIngredient2 != null) {
			cmbIngredient2.removeAllItems();
		}

		aIngredientList = ingredientdao.getIngredientList(datasource);

		for (int i = 0; i < aIngredientList.size(); i++) {
			ingredient = aIngredientList.get(i);
			cmbIngredient2.addItem(ingredient.getIngredientName());
		}
		cmbIngredient2.setSelectedIndex(-1);

		// AutoCompletion.enable(cmbIngredient);
	}

	// ---------------------------cmbIngredient
	private void getListOfStrength() {
		if (cmbStrength2 != null) {
			cmbStrength2.removeAllItems();
		}

		aIngredientStrengthList = ingredientstrengthdao.getIngredientStrengthList(datasource);

		for (int i = 0; i < aIngredientStrengthList.size(); i++) {
			ingredientstrength = aIngredientStrengthList.get(i);
			cmbStrength2.addItem(ingredientstrength.getStrength());
		}
		cmbStrength2.setSelectedIndex(0);
	}

	// ---------------------------
	private void getListMimsLevel1() {
		if (cmbMimsLevel1 != null) {
			cmbMimsLevel1.removeAllItems();
		}

		aListMimsLevel1 = mimslevel1dao.getMimsLevel1List(datasource);

		for (int i = 0; i < aListMimsLevel1.size(); i++) {
			mimslevel1 = aListMimsLevel1.get(i);
			cmbMimsLevel1.addItem(mimslevel1.getMimsLevel1Name());
		}
		panel_36.add(cmbMimsLevel1);
		cmbMimsLevel1.setSelectedIndex(0);
		
				JLabel lblNewLabel_2 = new JLabel("MIMS Level 2 :");
				lblNewLabel_2.setFont(new Font("Verdana", Font.PLAIN, 11));
				lblNewLabel_2.setForeground(new Color(255, 255, 255));
				panel_36.add(lblNewLabel_2);
	}

	// ---------------------------
	private void getListMimsLevel2() {
		if (cmbMimsLevel2 != null) {
			cmbMimsLevel2.removeAllItems();
		}

		aListMimsLevel2 = mimslevel2dao.getMimsLevel2List(datasource);

		for (int i = 0; i < aListMimsLevel2.size(); i++) {
			mimslevel2 = aListMimsLevel2.get(i);
			cmbMimsLevel2.addItem(mimslevel2.getMimsLevel2Name());
		}
		panel_36.add(cmbMimsLevel2);
		cmbMimsLevel2.setSelectedIndex(0);
		
				JLabel lblNewLabel_3 = new JLabel("MIMS Level 3 :");
				lblNewLabel_3.setFont(new Font("Verdana", Font.PLAIN, 11));
				lblNewLabel_3.setForeground(new Color(255, 255, 255));
				panel_36.add(lblNewLabel_3);
	}

	// ---------------------------
	private void getListMimsLevel3() {
		if (cmbMimsLevel3 != null) {
			cmbMimsLevel3.removeAllItems();
		}

		aListMimsLevel3 = mimslevel3dao.getMimsLevel3List(datasource);

		for (int i = 0; i < aListMimsLevel3.size(); i++) {
			mimslevel3 = aListMimsLevel3.get(i);
			cmbMimsLevel3.addItem(mimslevel3.getMimsLevel3Name());
		}
		panel_36.add(cmbMimsLevel3);
		cmbMimsLevel3.setSelectedIndex(0);
	}

	// ----------------------------arProductForm
	private void getListOfProductForms() {
		aProductFormList = productformdao.getProductFormList(datasource);

		for (int i = 0; i < aProductFormList.size(); i++) {
			productform = aProductFormList.get(i);
			cmbProductForm.addItem(productform.getProductFormDesc());
		}
	}

	// -------------------
	private String getSupplierCode(final int pIdSupplier) {
		String code = "";
		for (int i = 0; i < aSuppList.size(); i++) {
			supplier = aSuppList.get(i);
			if (supplier.getIdSupplier() == pIdSupplier) {
				code = supplier.getSupplierCode();
			}
		}

		return code;
	}

	// ----------------------------Manufacturers
	private void getListOfManufacturers() {
		List<Manufacturer> aManufacturerList = manufacturerdao.getManufacturerList(datasource);
		arManufacturer = new String[aManufacturerList.size()];

		for (int i = 0; i < aManufacturerList.size(); i++) {
			manufacturer = aManufacturerList.get(i);
			arManufacturer[i] = manufacturer.getManufacturerName();
		}
	}

	// -----------
	private boolean insertStockItem(final int pCalledFrom) {
		boolean isValid = false;
		String prodNo = edtStockItemProdNo.getText().toUpperCase();
		String prodName = edtStockItemDesc.getText().toUpperCase();

		if (prodNo.trim().equals("") || prodName.trim().equals("")) {
			String message = "Product# or Product name not entered";
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
		} else {
			int idProductDetail = numericutil.str2Int(edtProductID.getText());
			if (pCalledFrom == 1 && idProductDetail < 1) {
				String message = "Product detail id not found";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			} else {
				String shortName = cmbSupplierList.getItemAt(cmbSupplierList.getSelectedIndex());
				if (shortName == null) {
					String message = "Supplier / Wholesaler not selected";
					JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
				} else {
					// insert in stock item table
					isValid = true;
					// only stockitem to be inserted
					if (pCalledFrom == 1) {
						sendStockItemToDb(idProductDetail);
						edtStockItemProdNo.setText("");
						edtStockItemDesc.setText("");
						cmbSupplierList.setSelectedIndex(-1);
					}
				}
			}
		}
		return isValid;
	}

	// ------------
	private boolean sendStockItemToDb(final int pIdProductDetail) {
		boolean isValid = false;

		String prodNo = edtStockItemProdNo.getText().toUpperCase();
		String prodName = edtStockItemDesc.getText().toUpperCase();

		StockItem stockitem2 = new StockItem();
		List<StockItem> aStockitemList = new ArrayList<StockItem>();

		String shortName = cmbSupplierList.getItemAt(cmbSupplierList.getSelectedIndex());
		int idSupp = 0;

		for (int i = 0; i < aSuppList.size(); i++) {
			supplier = aSuppList.get(i);
			if (supplier.getSupplierShortName().equals(shortName)) {
				idSupp = supplier.getIdSupplier();
			}
		}

		stockitem2.setIdProduct(pIdProductDetail);
		stockitem2.setIdSupplier(idSupp);
		stockitem2.setStockItemProdNo(prodNo);
		stockitem2.setStockItemName(prodName);
		stockitem2.setStockItemPack("");
		stockitem2.setStockItemSellingPrice(new BigDecimal("0"));
		stockitem2.setStockItemSoh(0);
		stockitem2.setStockItemAgentOut("N");
		stockitem2.setExpDate1("");
		stockitem2.setExpDate2("");
		stockitem2.setExpDate3("");
		stockitem2.setSellingModule("");
		stockitem2.setDiscontinued("N");

		aStockitemList.add(stockitem2);

		StringRepresentation strRepresent = stockitemtoxml.buildXmlRecord(aStockitemList);
		ClientResource requestResource = null;
		try {
			// log.info(" >>>>Start>>>>>>>>>>>>>>>>>>>>>>POST>>>>>");
			String http = SERVER_HTTP + "/" + "stockitem/add/";

			requestResource = new ClientResource(http);
			// Representation representation;
			// representation = new StringRepresentation(xml.toString());
			strRepresent.setMediaType(MediaType.APPLICATION_XML);
			Representation reply = requestResource.post(strRepresent);

			int returnStatus = requestResource.getStatus().getCode();

			String replyText = reply.getText();
			log.info("Reply Text:" + replyText);

			if (returnStatus == 201) {
				// System.out.println(">>>>>>"+requestResource.getText());
				requestResource.release();
				isValid = true;
				String message = "Insert was successful!!!!!!";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.INFORMATION_MESSAGE);
			} else {
				String message = "Insert Error : " + replyText;
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			}
		} catch (ResourceException e) {
			// Something is wrong.
			int returnStatus = requestResource.getStatus().getCode();
			if (returnStatus == 403) {
				String message = "Insert Error : StockItem already exist";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			} else {
				String message = "Insert Error : " + returnStatus;
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			}
			// e.printStackTrace();
		} catch (Exception e) {
			// Something is wrong.
			e.printStackTrace();
		}
		return isValid;
	}

	// ---------------------
	private Product getProductDetail() {

		boolean isValid = true;

		Product product2 = new Product();

		String schedule = prodSchedOptions[cmbProductSched.getSelectedIndex()];
		String prodType = productTypeOptionsMeaning[cmbProductType.getSelectedIndex()];

		if (prodType.equals("E = Ethical Products")) {
			prodType = "E";
		}
		if (prodType.equals("S = Surgical Products")) {
			prodType = "S";
		}

		if (prodType.equals("O = Other")) {
			prodType = "O";
		}
		if (prodType.equals("EO = Ethical Original")) {
			prodType = "EO";
		}
		if (prodType.equals("EG = Ethical Generic")) {
			prodType = "EG";
		}
		if (prodType.equals("EC = Ethical Clone")) {
			prodType = "EC";
		}
		if (prodType.equals("V = Veterinary Products")) {
			prodType = "V";
		}

		productform = aProductFormList.get(cmbProductForm.getSelectedIndex());
		int idProdForm = productform.getIdProductForm();
		String manufactName = arManufacturer[cmbManufacturer.getSelectedIndex()];
		manufacturer = manufacturerdao.getManufacturerOnName(manufactName, datasource);

		product2.setIdProduct(edtProductID.getText());

		product2.setIdManufacturer(manufacturer.getIdManufacturer());
		product2.setProductName(edtproductName.getText().toUpperCase());
		product2.setProductPack(edtProductPack.getText());
		product2.setProductDescription(edtProductDescrip.getText());
		product2.setProductBarCode(edtProductBarcode.getText());
		product2.setProductNappiCode(edtProductNappicode.getText());
		product2.setProductSchedule(schedule);
		product2.setDiscontinued("N");
		product2.setProductStrength(edtProductStrength.getText());
		product2.setProductType(prodType);
		product2.setCatalogueNumber(edtCatalogueNum.getText());
		product2.setDosageForm(edtDosageForm.getText());
		product2.setIdProductForm(idProdForm);

		// product fields not completed - can not submit
		if (!isValid) {
			product2 = null;
		}
		return product2;

	}

	// ------------
	private void sendProductUpdateToDb(final Product pProduct) {

		List<Product> aProduct = new ArrayList<Product>();
		ProductToXml producttoxml = new ProductToXml();

		aProduct.add(pProduct);

		StringRepresentation strRepresent = producttoxml.buildXmlRecord(aProduct);
		ClientResource requestResource = null;
		try {
			// log.info(" >>>>Start>>>>>>>>>>>>>>>>>>>>>>POST>>>>>");
			String http = SERVER_HTTP + "/" + "products/update/";

			requestResource = new ClientResource(http);
			// Representation representation;
			// representation = new StringRepresentation(xml.toString());
			strRepresent.setMediaType(MediaType.APPLICATION_XML);
			Representation reply = requestResource.put(strRepresent);

			int returnStatus = requestResource.getStatus().getCode();

			// String replyText = reply.getText();
			// log.info("Reply Text:" + replyText);

			if (returnStatus == 204) {
				// System.out.println(">>>>>>"+requestResource.getText());
				requestResource.release();
				String message = "Update was successful!!!!!!";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.INFORMATION_MESSAGE);
			} else {
				String message = "Update Error : ";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			// Something is wrong.
			int returnStatus = requestResource.getStatus().getCode();
			String message = "Update Error : " + returnStatus;
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	// ---------------------
	private void sendNewProductToDb(final Product pProduct) {
		int returnId = 0;
		String replyText = "";
		pProduct.setNameUsed("R");
		List<Product> aProduct = new ArrayList<Product>();
		ProductToXml producttoxml = new ProductToXml();

		aProduct.add(pProduct);

		StringRepresentation strRepresent = producttoxml.buildXmlRecord(aProduct);
		ClientResource requestResource = null;
		try {
			// log.info(" >>>>Start>>>>>>>>>>>>>>>>>>>>>>>>>");
			String http = SERVER_HTTP + "/" + "products/add/";

			requestResource = new ClientResource(http);
			// Representation representation;
			// representation = new StringRepresentation(xml.toString());
			strRepresent.setMediaType(MediaType.APPLICATION_XML);
			Representation reply = requestResource.post(strRepresent);

			int returnStatus = requestResource.getStatus().getCode();

			replyText = reply.getText();
			// log.info("Reply Text:" + replyText);

			if (returnStatus == 201) {
				// System.out.println(">>>>>>"+requestResource.getText());
				requestResource.release();
				returnId = numericutil.str2Int(replyText);

				// insert stockitem after product inserted
				sendStockItemToDb(returnId);

				// - String message = "Insert was successful!!!!!!";
				// JOptionPane.showMessageDialog(new JFrame(), message,
				// "Dialog", JOptionPane.INFORMATION_MESSAGE);
			} else {
				String message = "Insert Error : ";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			// Something is wrong.
			int returnStatus = requestResource.getStatus().getCode();
			String message = "";
			if (returnStatus == 403) {
				message = "Insert Error: Product already exists need to be updated";
			} else {
				message = "Insert Error : " + returnStatus;
			}
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
		}
	}

	// -------------------------------
	private void searchProdOnId() {
		ClientResource requestResource = null;

		int prodId = numericutil.str2Int(edtProductID.getText());

		if (prodId > 0) {
			// get product for id supplied
			try {
				String http = SERVER_HTTP + "/products/getid/idproduct/" + prodId;

				requestResource = new ClientResource(http);
				Representation reply = requestResource.get();

				int returnStatus = requestResource.getStatus().getCode();
				if (returnStatus == 200) {
					requestResource.release();
					SaxRepresentation sax = new SaxRepresentation(reply);
					// xml to Product object
					// String replyText = reply.getText();
					// System.out.println("Reply Text:" + replyText);

					List<Product> aProdList = productfromxml.getProductData(sax);
					if (aProdList.size() > 0) {
						displayValuesWithName(aProdList.get(0));
					}
				}
			} catch (ResourceException e) {
				String message = "Product not found on entered ID";
				JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			String message = "Product not found on entered ID";
			JOptionPane.showMessageDialog(new JFrame(), message, "Dialog", JOptionPane.ERROR_MESSAGE);
		}
	}

	// ------------------------------
	private void linkIngToProduct() {
		String message1 = "Position in use";
		String message2 = "Ingredient Allready exist can not add!!!";
		int ingPos = (Integer) spnrIngPoition.getValue();

		if (arrIngPos[ingPos] == 1) {
			JOptionPane.showMessageDialog(new JFrame(), message1, "Dialog", JOptionPane.ERROR_MESSAGE);
		} else {
			int pos = cmbActiveIngredient.getSelectedIndex();
			activeingredients = aActiveIngredientsList.get(pos);
			int idProductDetail = numericutil.str2Int(edtProductID.getText());

			ArrayList<ProductHasActiveIngredients> aList = producthasactiveingredientsdao
					.getIngrForProd(idProductDetail, activeingredients.getIdActiveIngredients(), datasource);
			if (aList.size() > 0) {
				JOptionPane.showMessageDialog(new JFrame(), message2, "Dialog", JOptionPane.ERROR_MESSAGE);
			} else {
				if (producthasactiveingredientsdao.insertProductHasActiveIngredients(idProductDetail,
						activeingredients.getIdActiveIngredients(), ingPos, datasource)) {
					Product product2 = productdao.getProductOnId(idProductDetail, datasource);
					buildIngredientTable(product2);
				}
			}
		}

	}

	// ---------------------------------objActiveIng[0]
	private void linkNewIngredient() {

		String message2 = "No selection made";
		String message3 = "Ingredient detail not found";
		String message4 = "Ingredient inserted";
		String message5 = "Ingredient insert error !!!!!!!!!!!!!";
		String message6 = "Can not add Ingredient, exist in list";

		int idActiveIngredients = 0;

		// insert new ingredient
		int ingrSelected = cmbIngredient2.getSelectedIndex();
		int strengthSelected = cmbStrength2.getSelectedIndex();

		if (ingrSelected >= 0 && strengthSelected >= 0) {
			ingredient = aIngredientList.get(ingrSelected);
			ingredientstrength = aIngredientStrengthList.get(strengthSelected);

			if (aListTableIngr.contains(ingredient.getIdIntgredient())) {
				JOptionPane.showMessageDialog(new JFrame(), message6, "Dialog", JOptionPane.ERROR_MESSAGE);
			} else {
				activeingredients = activeingredientsdao.getIngredientId(ingredient.getIdIntgredient(),
						ingredientstrength.getIdIngredientStrength(), datasource);

				if (activeingredients == null) {
					// does not exist insert
					idActiveIngredients = activeingredientsdao.insertActiveIngredients(ingredient.getIdIntgredient(),
							ingredientstrength.getIdIngredientStrength(), datasource);
					buildIngredientListTable();
					getListOfActiveIngredient();
					JOptionPane.showMessageDialog(new JFrame(), message4, "Dialog", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(new JFrame(), message6, "Dialog", JOptionPane.ERROR_MESSAGE);
				}
				// if (idActiveIngredients > 0) {
				// int idProductDetail =
				// numericutil.str2Int(edtProductID.getText());
				// if
				// (producthasactiveingredientsdao.insertProductHasActiveIngredients(idProductDetail,
				// idActiveIngredients, ingPos, datasource)) {
				// Product product2 = productdao.getProductOnId(idProductDetail,
				// datasource);
				// buildIngredientTable(product2);
				// JOptionPane.showMessageDialog(new JFrame(), message4,
				// "Dialog", JOptionPane.ERROR_MESSAGE);
				// } else {
				// JOptionPane.showMessageDialog(new JFrame(), message5,
				// "Dialog", JOptionPane.ERROR_MESSAGE);
				// }
				// } else {
				// JOptionPane.showMessageDialog(new JFrame(), message3,
				// "Dialog", JOptionPane.ERROR_MESSAGE);
				// }
			}
		} else {
			JOptionPane.showMessageDialog(new JFrame(), message2, "Dialog", JOptionPane.ERROR_MESSAGE);
		}

	}

	// ----------------------------------
	private void buildIngredientTable(final Product pProduct) {
		// active ingredient
		tableModelActiveIng.setRowCount(0);

		if (aProductHasIng != null) {
			aProductHasIng.clear();
		}

		if (aListTableIngr != null) {
			aListTableIngr.clear();
		}
		if (aListIdActiveIngr != null) {
			aListIdActiveIngr.clear();
		}

		Arrays.fill(arrIngPos, 0);

		aProductHasIng = producthasactiveingredientsdao.getProductActiveIngList(pProduct.getIdProduct(), datasource);

		for (int i = 0; i < aProductHasIng.size(); i++) {
			producthasactiveingredients = aProductHasIng.get(i);
			activeingredients = activeingredientsdao
					.getIngredientOnId(producthasactiveingredients.getIdActiveIngredients(), datasource);
			Object[] objActiveIng = new Object[4];
			aListIdActiveIngr.add(activeingredients.getIdActiveIngredients());

			objActiveIng[0] = producthasactiveingredients.getIngSeq();
			arrIngPos[producthasactiveingredients.getIngSeq()] = 1;
			if (activeingredients.getIdIngredient() == 0) {
				objActiveIng[1] = activeingredients.getIngredients();
				objActiveIng[2] = "";
			} else {
				objActiveIng[1] = activeingredients.getIngredientName();
				objActiveIng[2] = activeingredients.getStrength();
				aListTableIngr.add(activeingredients.getIdIngredient());
			}
			objActiveIng[3] = producthasactiveingredients.getIdActiveIngredients();
			tableModelActiveIng.addRow(objActiveIng);

		} // for
	}

	// -----------------------------
	private void removeActiveIngredient() {
		String message1 = "Ingredient Removed";
		String message2 = "Error in removing Ingredient";

		int rowIndex = tblActiveIngredient.getSelectedRow();

		int idProductDetail = numericutil.str2Int(edtProductID.getText());

		if (producthasactiveingredientsdao.deleteProductHasActiveIngredients(idProductDetail,
				aListIdActiveIngr.get(rowIndex), datasource)) {

		} else {
			JOptionPane.showMessageDialog(new JFrame(), message2, "Dialog", JOptionPane.ERROR_MESSAGE);
		}
	}

	// ---------------------------aListIdActiveIngrList
	private void getListOfActiveIngredient() {
		if (cmbActiveIngredient != null) {
			cmbActiveIngredient.removeAllItems();
		}

		if (aActiveIngredientsList != null) {
			for (int i = 0; i < aActiveIngredientsList.size(); i++) {
				activeingredients = aActiveIngredientsList.get(i);
				cmbActiveIngredient
						.addItem(activeingredients.getIngredientName() + " " + activeingredients.getStrength());
			}
			cmbActiveIngredient.setSelectedIndex(-1);
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		if(e.getSource()==edtProductNameSearch1) {
			
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {

				if (edtProductNameSearch1.isCursorSet()) {
					
					searchProduct();
				}
				
			}

			
		}
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
