package za.co.maintainproduct;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;

import za.co.smartpharmalib.datasource.Product;
import za.co.smartpharmalib.datasource.StockItem;

import java.awt.TextArea;
import java.awt.Color;
import javax.swing.border.TitledBorder;

public class DisplayProductInfo extends JDialog implements ActionListener {

	static Logger log = Logger.getLogger(DisplayProductInfo.class.getName());

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	private JButton btnCancel = new JButton("Cancel");
	private JButton btnDoLink = new JButton("Change Link");

	private JTextArea txtProdDesc = new JTextArea();
	private JTextField txtProdId;
	private JTextField txtBarcode;
	private JTextField txtNappi;
	private JTextField txtProductName;
	private JTextField txtPackSize;
	private JTextField txtDosageForm;
	private JTextField txtStrength;
	private JTextField txtProdForm;
	private JTextField txtSchedule;

	private boolean doLink = false;
	private JTextField edtWS;
	private JTextField edtProductCode;
	private JTextField edtProdName;

	// --------------------
	public  DisplayProductInfo(final JFrame pFrame, final Product pProduct, final StockItem pStockitem, final String pWsCode) {

		super(pFrame, true);
		setSize(655, 528);
		setLocationRelativeTo(pFrame);

		getContentPane().setLayout(null);

		btnCancel.addActionListener(this);
		btnDoLink.addActionListener(this);

		JLabel lplProdId = new JLabel("Product ID :");
		lplProdId.setHorizontalAlignment(SwingConstants.TRAILING);
		lplProdId.setBounds(53, 11, 89, 14);
		getContentPane().add(lplProdId);

		txtProdId = new JTextField();
		txtProdId.setEnabled(true);
		txtProdId.setEditable(false);
		txtProdId.setColumns(10);
		txtProdId.setBounds(148, 11, 71, 20);
		getContentPane().add(txtProdId);

		JLabel lblBarcode = new JLabel("Product Barcode :");
		lblBarcode.setHorizontalAlignment(SwingConstants.TRAILING);
		lblBarcode.setBounds(21, 42, 121, 14);
		getContentPane().add(lblBarcode);

		txtBarcode = new JTextField();
		txtBarcode.setEditable(false);
		txtBarcode.setColumns(10);
		txtBarcode.setBounds(148, 42, 191, 20);
		getContentPane().add(txtBarcode);

		JLabel lblNappi = new JLabel("Product Nappicode :");
		lblNappi.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNappi.setBounds(21, 67, 121, 14);
		getContentPane().add(lblNappi);

		txtNappi = new JTextField();
		txtNappi.setEditable(false);
		txtNappi.setColumns(10);
		txtNappi.setBounds(148, 67, 192, 20);
		getContentPane().add(txtNappi);

		JLabel lblProdName = new JLabel("Product Name : ");
		lblProdName.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProdName.setBounds(21, 94, 121, 14);
		getContentPane().add(lblProdName);

		txtProductName = new JTextField();
		txtProductName.setEditable(false);
		txtProductName.setColumns(10);
		txtProductName.setBounds(148, 91, 379, 20);
		getContentPane().add(txtProductName);

		JLabel lblProdDesc = new JLabel("Product Description :");
		lblProdDesc.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProdDesc.setBounds(21, 136, 121, 14);
		getContentPane().add(lblProdDesc);

		TextArea textArea = new TextArea();
		textArea.setEditable(false);
		textArea.setBounds(148, 120, 424, 76);
		getContentPane().add(textArea);

		JLabel lblPackSize = new JLabel("Pack  Size:");
		lblPackSize.setHorizontalAlignment(SwingConstants.TRAILING);
		lblPackSize.setBounds(21, 201, 121, 14);
		getContentPane().add(lblPackSize);

		txtPackSize = new JTextField();
		txtPackSize.setEditable(false);
		txtPackSize.setColumns(10);
		txtPackSize.setBounds(148, 201, 69, 20);
		getContentPane().add(txtPackSize);

		JLabel lblDosage = new JLabel("Dosage Form :");
		lblDosage.setHorizontalAlignment(SwingConstants.TRAILING);
		lblDosage.setBounds(246, 201, 77, 14);
		getContentPane().add(lblDosage);

		txtDosageForm = new JTextField();
		txtDosageForm.setEditable(false);
		txtDosageForm.setColumns(10);
		txtDosageForm.setBounds(333, 198, 70, 20);
		getContentPane().add(txtDosageForm);

		JLabel lblProdForm = new JLabel("Product Form :");
		lblProdForm.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProdForm.setBounds(10, 232, 132, 14);
		getContentPane().add(lblProdForm);

		JLabel lblSchedule = new JLabel("Product Schedule :");
		lblSchedule.setHorizontalAlignment(SwingConstants.TRAILING);
		lblSchedule.setBounds(21, 257, 121, 14);
		getContentPane().add(lblSchedule);

		JLabel lblStrength = new JLabel("Product Strength :");
		lblStrength.setHorizontalAlignment(SwingConstants.TRAILING);
		lblStrength.setBounds(21, 282, 121, 14);
		getContentPane().add(lblStrength);

		txtStrength = new JTextField();
		txtStrength.setEditable(false);
		txtStrength.setColumns(10);
		txtStrength.setBounds(148, 279, 69, 20);
		getContentPane().add(txtStrength);

		txtProdForm = new JTextField();
		txtProdForm.setEditable(false);
		txtProdForm.setBounds(148, 229, 180, 20);
		getContentPane().add(txtProdForm);
		txtProdForm.setColumns(10);

		txtSchedule = new JTextField();
		txtSchedule.setEditable(false);
		txtSchedule.setBounds(148, 254, 54, 20);
		getContentPane().add(txtSchedule);
		txtSchedule.setColumns(10);
		btnDoLink.setBounds(145, 455, 113, 23);
		getContentPane().add(btnDoLink);

		btnCancel.setBounds(299, 455, 89, 23);
		getContentPane().add(btnCancel);

		txtProdId.setText("" + pProduct.getIdProduct());
		txtProductName.setText(pProduct.getProductName());
		txtPackSize.setText(pProduct.getProductPack());
		txtDosageForm.setText(pProduct.getDosageForm());
		txtStrength.setText(pProduct.getProductStrength());
		txtBarcode.setText(pProduct.getProductBarCode());
		txtNappi.setText(pProduct.getProductNappiCode());
		txtSchedule.setText(pProduct.getProductSchedule());

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "StockItem to re-link", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBackground(Color.GRAY);
		panel.setBounds(21, 333, 608, 111);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Wholesaler :");
		lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNewLabel.setBounds(10, 24, 96, 14);
		panel.add(lblNewLabel);

		JLabel lblProductCode = new JLabel("Product Code :");
		lblProductCode.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProductCode.setBounds(10, 49, 96, 14);
		panel.add(lblProductCode);

		JLabel lblProductName = new JLabel("Product Name :");
		lblProductName.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProductName.setBounds(10, 74, 96, 14);
		panel.add(lblProductName);

		edtWS = new JTextField();
		edtWS.setEditable(false);
		edtWS.setBounds(116, 21, 86, 20);
		panel.add(edtWS);
		edtWS.setColumns(10);

		edtProductCode = new JTextField();
		edtProductCode.setEditable(false);
		edtProductCode.setBounds(116, 46, 86, 20);
		panel.add(edtProductCode);
		edtProductCode.setColumns(10);

		edtProdName = new JTextField();
		edtProdName.setEditable(false);
		edtProdName.setBounds(116, 71, 447, 20);
		panel.add(edtProdName);
		edtProdName.setColumns(10);
		txtProdDesc.setText(pProduct.getProductDescription());

		edtWS.setText(pWsCode);
		edtProductCode.setText(pStockitem.getStockItemProdNo());
		edtProdName.setText(pStockitem.getStockItemName());

		setVisible(true);

	}

	// --------------------------------------------------
	public final void actionPerformed(final ActionEvent e) {

		if (e.getSource() == btnCancel) {
			doLink = false;
			setVisible(false);
		}

		if (e.getSource() == btnDoLink) {
			doLink = true;
			setVisible(false);
		}
	}
	//---------------------------
	  public final boolean getDoLink()   {
	       return doLink;
	   }
}