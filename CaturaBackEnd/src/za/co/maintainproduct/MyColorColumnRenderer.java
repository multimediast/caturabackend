package za.co.maintainproduct;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

// -----------------------------------------------------------------------
/**
 * Applied background and foreground color to single column of a JTable
 */
class MyColorColumnRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	//private Color bkgndColor = Color.GREEN;
	//private Color fgndColor = Color.BLACK;

	// -------------
	public MyColorColumnRenderer() {
		super();
	}

	// -----------------
	public Component getTableCellRendererComponent(JTable table, Object pValue, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, pValue, isSelected, hasFocus, row, column);

		// only special columns
		if (column == 5) {
			String flag = (String) pValue;
			if (flag != null) {
				if (flag.equals("Y")) {
					cell.setBackground(Color.GREEN);
					cell.setForeground(Color.BLACK);
				} else {
					cell.setBackground(Color.WHITE);
					cell.setForeground(Color.BLACK);
				}
			}

		}

		return cell;
	}
}
