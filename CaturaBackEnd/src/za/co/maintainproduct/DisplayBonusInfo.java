package za.co.maintainproduct;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import za.co.smartpharmalib.datasource.StockItemBonus;
import za.co.smartpharmalib.datasource.StockItemBonusDao;
import za.co.smartpharmalib.datasource.StockItemBonusImpl;
import za.co.smartutillib.util.DateUtil;
import za.co.smartutillib.util.NumericUtil;

import java.awt.BorderLayout;

import javax.sql.DataSource;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class DisplayBonusInfo  extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;

	private StockItemBonusDao stockitembonusdao = new StockItemBonusImpl();
	private StockItemBonus stockitembonus = new StockItemBonus();
	private NumericUtil numericutil = new NumericUtil();
	private List<StockItemBonus> aList = null;

	private JButton btnReturn = new JButton("Return");
	private JLabel lblFromDate = new JLabel("From Date");
	private JLabel lblFromQty1 = new JLabel("");
	private JLabel lblFromQty2 = new JLabel("");
	private JLabel lblFromQty3 = new JLabel("");
	private JLabel lblFromQty4 = new JLabel("");

	private JLabel lblQtyTo1 = new JLabel("");
	private JLabel lblQtyTo2 = new JLabel("");
	private JLabel lblQtyTo3 = new JLabel("");
	private JLabel lblQtyTo4 = new JLabel("");

	private JLabel lblBonusQty1 = new JLabel("");
	private JLabel lblBonusQty2 = new JLabel("");
	private JLabel lblBonusQty3 = new JLabel("");
	private JLabel lblBonusQty4 = new JLabel("");

	private JLabel lblPrice1 = new JLabel("");
	private JLabel lblPrice2 = new JLabel("");
	private JLabel lblPrice3 = new JLabel("");
	private JLabel lblPrice4 = new JLabel("");

	private JLabel lblFromDate1 = new JLabel("");
	private JLabel lblFromDate2 = new JLabel("");
	private JLabel lblFromDate3 = new JLabel("");
	private JLabel lblFromDate4 = new JLabel("");

	private JLabel lblToDate1 = new JLabel("");
	private JLabel lblToDate2 = new JLabel("");
	private JLabel lblToDate3 = new JLabel("");
	private JLabel lblToDate4 = new JLabel("");

	// --------------------
	public DisplayBonusInfo(JFrame frame, String pProdName, int pIdStockitem, int pIdBonusClass, DataSource pDatasource) {
		super(frame, true);
		setTitle("Bonus & SpecialPrice : " + pProdName);
		setSize(646, 300 );

		aList = stockitembonusdao.getStockItemBonus(pIdStockitem, pIdBonusClass, pDatasource);

		btnReturn.addActionListener(this);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.activeCaption);
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);


		btnReturn.setBounds(220, 227, 89, 23);
		panel_1.add(btnReturn);

		JLabel lblSpecial = new JLabel("Special 1 :");
		lblSpecial.setBounds(22, 36, 58, 14);
		panel_1.add(lblSpecial);

		JLabel lblNewLabel = new JLabel("Special 2 :");
		lblNewLabel.setBounds(22, 86, 58, 14);
		panel_1.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Special 3 :");
		lblNewLabel_1.setBounds(23, 130, 57, 14);
		panel_1.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Special 4 :");
		lblNewLabel_2.setBounds(23, 169, 57, 14);
		panel_1.add(lblNewLabel_2);

		lblFromDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFromDate.setForeground(Color.WHITE);
		lblFromDate.setBounds(403, 11, 67, 14);
		panel_1.add(lblFromDate);

		JLabel lblToDate = new JLabel("To Date");
		lblToDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblToDate.setForeground(Color.WHITE);
		lblToDate.setBounds(512, 11, 73, 14);
		panel_1.add(lblToDate);

		JLabel lblToQty = new JLabel("To Qty");
		lblToQty.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblToQty.setForeground(Color.WHITE);
		lblToQty.setBounds(166, 11, 67, 14);
		panel_1.add(lblToQty);

		JLabel lblBonusQty = new JLabel("Bonus Qty");
		lblBonusQty.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBonusQty.setForeground(Color.WHITE);
		lblBonusQty.setBounds(247, 11, 67, 14);
		panel_1.add(lblBonusQty);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setForeground(Color.WHITE);
		lblPrice.setBounds(324, 11, 73, 14);
		panel_1.add(lblPrice);

		JPanel panel = new JPanel();
		panel.setBounds(89, 24, 71, 171);
		panel_1.add(panel);
		panel.setLayout(null);

		lblFromQty1.setBounds(10, 11, 51, 14);
		panel.add(lblFromQty1);

		lblFromQty2.setBounds(10, 62, 51, 14);
		panel.add(lblFromQty2);

		lblFromQty3.setBounds(10, 107, 51, 14);
		panel.add(lblFromQty3);

		lblFromQty4.setBounds(10, 146, 51, 14);
		panel.add(lblFromQty4);

		JLabel lblFromQty = new JLabel("From Qty");
		lblFromQty.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFromQty.setForeground(Color.WHITE);
		lblFromQty.setBounds(89, 11, 67, 14);
		panel_1.add(lblFromQty);

		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(166, 24, 71, 171);
		panel_1.add(panel_2);

		lblQtyTo1.setBounds(10, 11, 51, 14);
		panel_2.add(lblQtyTo1);

		lblQtyTo2.setBounds(10, 62, 51, 14);
		panel_2.add(lblQtyTo2);

		lblQtyTo3.setBounds(10, 107, 51, 14);
		panel_2.add(lblQtyTo3);

		lblQtyTo4.setBounds(10, 146, 51, 14);
		panel_2.add(lblQtyTo4);

		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBounds(247, 24, 71, 171);
		panel_1.add(panel_3);

		lblBonusQty1.setBounds(10, 11, 51, 14);
		panel_3.add(lblBonusQty1);

		lblBonusQty2.setBounds(10, 62, 51, 14);
		panel_3.add(lblBonusQty2);

		lblBonusQty3.setBounds(10, 107, 51, 14);
		panel_3.add(lblBonusQty3);

		lblBonusQty4.setBounds(10, 146, 51, 14);
		panel_3.add(lblBonusQty4);

		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBounds(326, 24, 77, 171);
		panel_1.add(panel_4);

		lblPrice1.setBounds(6, 11, 65, 14);
		panel_4.add(lblPrice1);

		lblPrice2.setBounds(6, 62, 65, 14);
		panel_4.add(lblPrice2);

		lblPrice3.setBounds(6, 107, 65, 14);
		panel_4.add(lblPrice3);

		lblPrice4.setBounds(6, 146, 65, 14);
		panel_4.add(lblPrice4);

		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBounds(413, 24, 99, 171);
		panel_1.add(panel_5);

		lblFromDate1.setBounds(10, 11, 79, 14);
		panel_5.add(lblFromDate1);

		lblFromDate2.setBounds(10, 62, 79, 14);
		panel_5.add(lblFromDate2);

		lblFromDate3.setBounds(10, 107, 79, 14);
		panel_5.add(lblFromDate3);

		lblFromDate4.setBounds(10, 146, 79, 14);
		panel_5.add(lblFromDate4);

		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBounds(522, 24, 99, 171);
		panel_1.add(panel_6);

		lblToDate1.setBounds(10, 11, 79, 14);
		panel_6.add(lblToDate1);

		lblToDate2.setBounds(10, 62, 79, 14);
		panel_6.add(lblToDate2);

		lblToDate3.setBounds(10, 107, 79, 14);
		panel_6.add(lblToDate3);

		lblToDate4.setBounds(10, 146, 79, 14);
		panel_6.add(lblToDate4);


		displayData();

		setVisible(true);

	}

	//------------------------------------------------
	private void displayData() {
		for (int i = 0; i < aList.size(); i++) {
			stockitembonus = aList.get(i);
			switch (i) {
			case 0:
				lblFromQty1.setText("" + stockitembonus.getBonusFromQty());
				lblQtyTo1.setText("" + stockitembonus.getBonusToQty());
				lblBonusQty1.setText("" + stockitembonus.getBonusQty());
				lblPrice1.setText(numericutil.toMoney(stockitembonus.getBonusPrice()));

				lblFromDate1.setText(DateUtil.sqlDateToString(stockitembonus.getBonusFromDate(), "dd-MM-yyyy"));
				lblToDate1.setText(DateUtil.sqlDateToString(stockitembonus.getBonusToDate(), "dd-MM-yyyy"));

				break;
			case 1:
				lblFromQty2.setText("" + stockitembonus.getBonusFromQty());
				lblQtyTo2.setText("" + stockitembonus.getBonusToQty());
				lblBonusQty2.setText("" + stockitembonus.getBonusQty());
				lblPrice2.setText(numericutil.toMoney(stockitembonus.getBonusPrice()));
				lblFromDate2.setText(DateUtil.sqlDateToString(stockitembonus.getBonusFromDate(), "dd-MM-yyyy"));
				lblToDate2.setText(DateUtil.sqlDateToString(stockitembonus.getBonusToDate(), "dd-MM-yyyy"));

				break;
			case 2:
				lblFromQty3.setText("" + stockitembonus.getBonusFromQty());
				lblQtyTo3.setText("" + stockitembonus.getBonusToQty());
				lblBonusQty3.setText("" + stockitembonus.getBonusQty());
				lblPrice3.setText(numericutil.toMoney(stockitembonus.getBonusPrice()));
				lblFromDate3.setText(DateUtil.sqlDateToString(stockitembonus.getBonusFromDate(), "dd-MM-yyyy"));
				lblToDate3.setText(DateUtil.sqlDateToString(stockitembonus.getBonusToDate(), "dd-MM-yyyy"));

				break;
			case 3:
				lblFromQty4.setText("" + stockitembonus.getBonusFromQty());
				lblQtyTo4.setText("" + stockitembonus.getBonusToQty());
				lblBonusQty4.setText("" + stockitembonus.getBonusQty());
				lblPrice4.setText(numericutil.toMoney(stockitembonus.getBonusPrice()));
				lblFromDate4.setText(DateUtil.sqlDateToString(stockitembonus.getBonusFromDate(), "dd-MM-yyyy"));
				lblToDate4.setText(DateUtil.sqlDateToString(stockitembonus.getBonusToDate(), "dd-MM-yyyy"));

				break;
			}
		}
	}
	//-----------------
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnReturn) {
			setVisible(false);
		}

	}
}
