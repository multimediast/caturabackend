package za.co.maintainproduct;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.sql.DataSource;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import za.co.smartpharmalib.datasource.BonusClass;
import za.co.smartpharmalib.datasource.BonusClassDao;
import za.co.smartpharmalib.datasource.BonusClassImpl;
import za.co.smartpharmalib.datasource.PriceClass;
import za.co.smartpharmalib.datasource.PriceClassDao;
import za.co.smartpharmalib.datasource.PriceClassImpl;
import za.co.smartpharmalib.datasource.StockItem;
import za.co.smartpharmalib.datasource.StockItemPrice;
import za.co.smartpharmalib.datasource.StockItemPriceDao;
import za.co.smartpharmalib.datasource.StockItemPriceImpl;
import za.co.smartutillib.util.DateUtil;
import za.co.smartutillib.util.NumericUtil;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.SystemColor;

public class DisplaySpecialInfo   extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JTable table;
	private JButton btnReturn = new JButton("Return");
	private DefaultTableModel tableModel = null;

	private StockItemPriceDao stockitempricedao = new StockItemPriceImpl();
	private StockItemPrice stockitemprice = new StockItemPrice();
	private NumericUtil numericutil = new NumericUtil();

	private PriceClassDao priceclassdao = new PriceClassImpl();
	private PriceClass priceclass = new PriceClass();

	private List<StockItemPrice> aList = null;

	// --------------------
	public DisplaySpecialInfo(final JFrame frame, final String pProdName, final int pIdStockItem, final int pIdSupplier, final DataSource pDatasource) {
		super(frame, true);
		setTitle("Account Special Price : " + pProdName);
		setSize(646, 300);
		getContentPane().setLayout(new BorderLayout(0, 0));

		btnReturn.addActionListener(this);

		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.activeCaption);
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		/*
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Class", "From Date", "To Date", "Plus/Min Amount", "Plus-Min %", "Fixed Price"
			}
		));
		*/
		String[] col = {"Class", "From Date", "To Date", "Plus/Min Amount", "Plus-Min %", "Fixed Price"};
		tableModel = new DefaultTableModel(col, 7);
		table = new JTable(tableModel);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBackground(SystemColor.activeCaption);
		panel.add(scrollPane);

		JPanel panel1 = new JPanel();
		panel1.setBackground(SystemColor.activeCaption);
		getContentPane().add(panel1, BorderLayout.SOUTH);

		panel1.add(btnReturn);

		  aList = stockitempricedao.getStockItemPriceForSupplier(
		            pIdSupplier, pIdStockItem, pDatasource);

		  buildData(pDatasource);

		setVisible(true);
	}

	//------------------------------------
	private void buildData(final DataSource pDatasource) {
		tableModel.setRowCount(1);

		for (int i = 0; i < aList.size(); i++) {
			stockitemprice = aList.get(i);
			priceclass = priceclassdao.getPriceClassOnId(stockitemprice.getIdPriceClass(), pDatasource);

			Object[] objects = new Object[6];
			objects[0] = priceclass.getPriceClassDesc();
			objects[1] = DateUtil.sqlDateToString(stockitemprice.getFromDate(), "dd-MM-yyyy");
			objects[2] = DateUtil.sqlDateToString(stockitemprice.getToDate(), "dd-MM-yyyy");
			objects[3] = numericutil.toMoney(stockitemprice.getPlusMinAmmount());
			objects[4] = stockitemprice.getPlusMinPercentage();
			objects[5] = numericutil.toMoney(stockitemprice.getFixedPrice());

			tableModel.addRow(objects);
		}

	}
	//---------------------
	@Override
	public void actionPerformed(final ActionEvent e) {
		if (e.getSource() == btnReturn) {
			setVisible(false);
		}

	}
}
