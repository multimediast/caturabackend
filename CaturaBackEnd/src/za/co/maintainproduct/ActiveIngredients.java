package za.co.maintainproduct;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;

public class ActiveIngredients extends JDialog
{
  private final JPanel contentPanel = new JPanel();
  

  public static void main(String[] args)
  {
    try
    {
      ActiveIngredients dialog = new ActiveIngredients();
      dialog.setDefaultCloseOperation(2);
      dialog.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  


  public ActiveIngredients()
  {
    setTitle("Active Ingredients");
    setBounds(100, 100, 530, 300);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, "Center");
    contentPanel.setLayout(null);
    
    JLabel lblNewLabel = new JLabel("New label");
    lblNewLabel.setBounds(61, 11, 46, 14);
    contentPanel.add(lblNewLabel);
    
    JLabel lblNewLabel_1 = new JLabel("New label");
    lblNewLabel_1.setBounds(61, 55, 46, 14);
    contentPanel.add(lblNewLabel_1);
    
    JLabel lblNewLabel_2 = new JLabel("New label");
    lblNewLabel_2.setBounds(61, 106, 46, 14);
    contentPanel.add(lblNewLabel_2);
    
    JLabel lblNewLabel_3 = new JLabel("New label");
    lblNewLabel_3.setBounds(61, 146, 46, 14);
    contentPanel.add(lblNewLabel_3);
    
    JLabel lblNewLabel_4 = new JLabel("New label");
    lblNewLabel_4.setBounds(61, 188, 46, 14);
    contentPanel.add(lblNewLabel_4);
    
    JComboBox comboBox = new JComboBox();
    comboBox.setBounds(128, 8, 376, 20);
    contentPanel.add(comboBox);
    
    JComboBox comboBox_1 = new JComboBox();
    comboBox_1.setBounds(128, 52, 376, 20);
    contentPanel.add(comboBox_1);
    
    JComboBox comboBox_2 = new JComboBox();
    comboBox_2.setBounds(128, 103, 376, 20);
    contentPanel.add(comboBox_2);
    
    JComboBox comboBox_3 = new JComboBox();
    comboBox_3.setBounds(128, 143, 376, 20);
    contentPanel.add(comboBox_3);
    
    JComboBox comboBox_4 = new JComboBox();
    comboBox_4.setBounds(128, 185, 376, 20);
    contentPanel.add(comboBox_4);
    
    JPanel buttonPane = new JPanel();
    buttonPane.setLayout(new FlowLayout(2));
    getContentPane().add(buttonPane, "South");
    
    JButton okButton = new JButton("OK");
    okButton.setActionCommand("OK");
    buttonPane.add(okButton);
    getRootPane().setDefaultButton(okButton);
    

    JButton cancelButton = new JButton("Cancel");
    cancelButton.setActionCommand("Cancel");
    buttonPane.add(cancelButton);
  }
}
