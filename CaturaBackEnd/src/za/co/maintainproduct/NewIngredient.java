package za.co.maintainproduct;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.sql.DataSource;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import za.co.smartpharmalib.datasource.IngredientDao;
import za.co.smartpharmalib.datasource.IngredientImpl;
import za.co.util.GlobalSettings;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;

public class NewIngredient extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;

	private IngredientDao ingredientdao = new IngredientImpl();
	private JTextField txtIngName;

	private JButton btnCancel = new JButton("Cancel");
	private JButton btnAdd = new JButton("Add");

	private DataSource datasource = null;
	private boolean isInserted = false;


	/**
	 * Create the dialog.
	 */
	public NewIngredient(final JFrame pFrame, DataSource pDatasource) {
		
		super(pFrame, true);
		
		datasource = pDatasource;
		getContentPane().setBackground(Color.LIGHT_GRAY);

		setBounds(100, 100, 450, 146);
		getContentPane().setLayout(new BorderLayout());

		JPanel buttonPane = new JPanel();
		buttonPane.setBackground(GlobalSettings.PANEL_BLUE);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnCancel.setFont(new Font("Verdana", Font.BOLD, 11));
		buttonPane.add(btnCancel);
		btnAdd.setForeground(new Color(255, 255, 255));
		btnAdd.setBackground(GlobalSettings.BUTTON_ORANGE);
		btnAdd.setFont(new Font("Verdana", Font.BOLD, 11));
		buttonPane.add(btnAdd);
		btnCancel.addActionListener(this);
		btnAdd.addActionListener(this);

		getRootPane().setDefaultButton(btnAdd);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(GlobalSettings.PANEL_BLUE);
		getContentPane().add(panel_1, BorderLayout.CENTER);
				
						JLabel lblNewLabel = new JLabel("New Ingredient :");
						lblNewLabel.setForeground(new Color(255, 255, 255));
						lblNewLabel.setFont(new Font("Verdana", Font.BOLD, 11));
		
				txtIngName = new JTextField();
				txtIngName.setColumns(40);
				GroupLayout gl_panel_1 = new GroupLayout(panel_1);
				gl_panel_1.setHorizontalGroup(
					gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(11)
							.addComponent(lblNewLabel)
							.addGap(5)
							.addComponent(txtIngName, GroupLayout.PREFERRED_SIZE, 302, GroupLayout.PREFERRED_SIZE)
							.addGap(24))
				);
				gl_panel_1.setVerticalGroup(
					gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(8)
							.addComponent(lblNewLabel))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(5)
							.addComponent(txtIngName, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
							.addContainerGap())
				);
				panel_1.setLayout(gl_panel_1);
		this.setVisible(true);
	}

	// ----------------------------------------------------
	// button listeners
	public final void actionPerformed(final ActionEvent e) {
		if (e.getSource() == btnCancel) {
			this.setVisible(false);
		}
		if (e.getSource() == btnAdd) {
			if (ingredientdao.InsertIngredient(txtIngName.getText().toUpperCase(), datasource)) {
				isInserted = true;
				JOptionPane.showMessageDialog(null, "New Ingredient inserted");
			} else {
				isInserted = false;
				JOptionPane.showMessageDialog(null, "Error Inserting new Ingredient!!!!!!!!!!");
			}
			this.setVisible(false);
		}
	}

	//------------------------
	public boolean isInserted() {
		return isInserted;
	}
}
