package za.co.main;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import za.co.maintainproduct.MaintainProduct;
import za.co.util.GlobalSettings;

@SuppressWarnings("serial")
public class Menu extends JFrame implements ActionListener{

	JButton btnSearchProduct = new JButton("");
	public JFrame pnlMenu;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private int width = 0;
	private int height = 0;
	private JTextField txtSystemUserName;

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		pnlMenu = new JFrame();
		pnlMenu.setTitle("Menu");
		pnlMenu.setBounds(100, 100, 602, 449);
		width = (int) screenSize.getWidth();
		height = (int) screenSize.getHeight();
		//height = 700; fix height screen
		pnlMenu.setSize(1253, 649);
		pnlMenu.setLocationRelativeTo(null);
		pnlMenu.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		JPanel panel = new JPanel();
		pnlMenu.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel pnlNorth = new JPanel();
		panel.add(pnlNorth, BorderLayout.NORTH);
		pnlNorth.setLayout(new BorderLayout(0, 0));

		JPanel pnlCenter = new JPanel();
		panel.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new BorderLayout(0, 0));

		JPanel pnlSubCenter = new JPanel();
		pnlCenter.add(pnlSubCenter, BorderLayout.CENTER);
		pnlSubCenter.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBackground(new Color(85, 142, 203));
		pnlSubCenter.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(new Color(85, 142, 203));
		FlowLayout flowLayout_3 = (FlowLayout) panel_5.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		panel_1.add(panel_5);
		
		JLabel lblBackend = new JLabel("");
		lblBackend.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_WHITE_LOGO));
		lblBackend.setForeground(new Color(255, 255, 255));
		lblBackend.setFont(new Font("Verdana", Font.BOLD, 36));
		panel_5.add(lblBackend);
		
		JLabel label = new JLabel("BackEnd");
		label.setForeground(new Color(255, 255, 255));
		label.setBackground(new Color(255, 255, 255));
		label.setFont(new Font("Verdana", Font.BOLD, 28));
		panel_5.add(label);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBackground(new Color(85, 142, 203));
		FlowLayout flowLayout_1 = (FlowLayout) panel_11.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panel_1.add(panel_11);
		
				JButton btnLogOut = new JButton("Logout");
				panel_11.add(btnLogOut);
				btnLogOut.setBackground(new Color(246, 141, 89));
				btnLogOut.setForeground(new Color(255, 255, 255));
				btnLogOut.setFont(new Font("Verdana", Font.BOLD, 12));
				btnLogOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						pnlMenu.dispose();
						CaturaBackEnd login = new CaturaBackEnd();
						login.pnlLogin.setVisible(true);
						System.exit(0);
					}
				});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_2.setBackground(new Color(255, 255, 255));
		
		panel_2.setEnabled(false);
		pnlSubCenter.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		txtSystemUserName = new JTextField();
		txtSystemUserName.setBackground(Color.WHITE);
		txtSystemUserName.setText(System.getProperty("user.name"));
		txtSystemUserName.setEnabled(false);
		txtSystemUserName.setEditable(false);
		txtSystemUserName.setFont(new Font("Verdana", Font.BOLD, 12));
		panel_2.add(txtSystemUserName);
		txtSystemUserName.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(85, 142, 203));
		pnlSubCenter.add(panel_3, BorderLayout.CENTER);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(new Color(85, 142, 203));
		FlowLayout flowLayout_2 = (FlowLayout) panel_8.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		panel_7.add(panel_8, BorderLayout.NORTH);
		
		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setForeground(new Color(255, 255, 255));
		lblMenu.setFont(new Font("Verdana", Font.BOLD, 24));
		panel_8.add(lblMenu);
		
		JPanel panel_9 = new JPanel();
		panel_7.add(panel_9, BorderLayout.SOUTH);
		panel_9.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_6 = new JPanel();
		panel_9.add(panel_6);
		panel_6.setBackground(new Color(255, 255, 255));
		panel_6.setLayout(new GridLayout(0, 4, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Maintain Product");
		lblNewLabel.setBackground(new Color(255, 255, 255));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Verdana", Font.BOLD, 16));
		panel_6.add(lblNewLabel);
		
		JLabel lblAddProduct = new JLabel("Add Product");
		lblAddProduct.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddProduct.setFont(new Font("Verdana", Font.BOLD, 16));
		panel_6.add(lblAddProduct);
		
		JLabel lblEditProduct = new JLabel("Edit Product");
		lblEditProduct.setHorizontalAlignment(SwingConstants.CENTER);
		lblEditProduct.setFont(new Font("Verdana", Font.BOLD, 16));
		panel_6.add(lblEditProduct);
		
		JLabel lblSccount = new JLabel("Account");
		lblSccount.setHorizontalAlignment(SwingConstants.CENTER);
		lblSccount.setFont(new Font("Verdana", Font.BOLD, 16));
		panel_6.add(lblSccount);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(new Color(255, 255, 255));
		panel_7.add(panel_10, BorderLayout.CENTER);
		panel_10.setLayout(new GridLayout(0, 4, 5, 5));
		panel_10.add(btnSearchProduct);
		btnSearchProduct.setBackground(new Color(246, 141, 89));
		
		btnSearchProduct.addActionListener(this);
		btnSearchProduct.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_SEARCH_ICON));
		
		JButton btnNewButton_3 = new JButton("");
		panel_10.add(btnNewButton_3);
		btnNewButton_3.setBackground(new Color(246, 141, 89));
		btnNewButton_3.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_ADD_ICON));
		
		JButton btnNewButton_1 = new JButton("");
		panel_10.add(btnNewButton_1);
		btnNewButton_1.setBackground(new Color(246, 141, 89));
		btnNewButton_1.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_EDIT_ICON));
		
		JButton btnNewButton_2 = new JButton("");
		panel_10.add(btnNewButton_2);
		btnNewButton_2.setBackground(new Color(246, 141, 89));
		btnNewButton_2.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_ACCOUNT_ICON));
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_3.add(panel_4);
		panel_3.add(panel_7);

		JPanel pnlSouth = new JPanel();
		panel.add(pnlSouth, BorderLayout.SOUTH);
		pnlSouth.setLayout(new BorderLayout(0, 0));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==btnSearchProduct){
			
			pnlMenu.dispose();
			MaintainProduct maintainproduct = new MaintainProduct();
			
			maintainproduct.setLocationRelativeTo(null);
			maintainproduct.pnlProduct.setExtendedState(JFrame.MAXIMIZED_BOTH);
			maintainproduct.pnlProduct.setVisible(true);
			
		}
		
	}
}
