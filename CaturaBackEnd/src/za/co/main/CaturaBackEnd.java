package za.co.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

import org.apache.log4j.Logger;

import za.co.smartutillib.util.GetProperties;
import za.co.util.GlobalSettings;

public class CaturaBackEnd implements ActionListener {

	private static Logger log = Logger.getLogger(CaturaBackEnd.class.getName());
	private GetProperties getproperties = new GetProperties();

	public JFrame pnlLogin;
	private JTextField edtUsername;

	JButton btnLogin = new JButton("Login");
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private int width = 0;
	private int height = 0;
	private JPasswordField edtPassword;

	/**
	 * Create the application.
	 */
	public CaturaBackEnd() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		getproperties.InitializeLogger();
		pnlLogin = new JFrame();
		pnlLogin.setTitle("CaturaBackEnd");
		pnlLogin.setBounds(100, 100, 594, 460);
		// width = (int) screenSize.getWidth();
		// height = (int) screenSize.getHeight();
		height = 500; // fix height screen
		width = 650;

		pnlLogin.setSize(554, 362);
		pnlLogin.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		JPanel panel = new JPanel();
		pnlLogin.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel pnlNorth = new JPanel();
		panel.add(pnlNorth, BorderLayout.NORTH);
		pnlNorth.setLayout(new BorderLayout(0, 0));

		JPanel pnlCenter = new JPanel();
		panel.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new BorderLayout(0, 0));

		JPanel pnlSubCenter = new JPanel();
		pnlCenter.add(pnlSubCenter, BorderLayout.CENTER);

		JPanel pnlSouth = new JPanel();
		panel.add(pnlSouth, BorderLayout.SOUTH);
		pnlSouth.setLayout(new BorderLayout(0, 0));

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlLogin.dispose();
			}
		});
		pnlSubCenter.setLayout(new BorderLayout(0, 0));
		pnlSubCenter.add(btnCancel);

		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(85, 142, 203));
		FlowLayout flowLayout_1 = (FlowLayout) panel_4.getLayout();
		flowLayout_1.setVgap(15);
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		pnlSubCenter.add(panel_4, BorderLayout.WEST);

		JLabel lblCatura = new JLabel("");
		lblCatura.setIcon(new ImageIcon(GlobalSettings.CATURABACKEND_CATURA_LOGO));
		lblCatura.setFont(new Font("Verdana", Font.BOLD, 36));
		panel_4.add(lblCatura);

		JPanel panel_5 = new JPanel();
		// panel_5.setOpaque(false);
		panel_5.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_5.setBackground(new Color(85, 142, 203));
		pnlSubCenter.add(panel_5);

		JSeparator separator = new JSeparator();

		JSeparator separator_1 = new JSeparator();

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(85, 142, 203));

		JPanel panel_6 = new JPanel();
		panel_6.setBackground(new Color(85, 142, 203));
		panel_6.setBorder(null);

		JSeparator separator_2 = new JSeparator();

		btnLogin.setBackground(new Color(246, 141, 89));
		btnLogin.setForeground(new Color(255, 255, 255));
		btnLogin.setFont(new Font("Verdana", Font.BOLD, 12));
		btnLogin.addActionListener(this);
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		gl_panel_5.setHorizontalGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_5.createSequentialGroup().addContainerGap()
						.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addGap(383))
				.addGroup(gl_panel_5.createSequentialGroup().addGap(56).addGroup(gl_panel_5
						.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnLogin, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
						.addComponent(separator_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
						.addComponent(panel_6, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
						.addComponent(separator, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
						.addComponent(panel_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE))
						.addGap(50)));
		gl_panel_5.setVerticalGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup().addGap(58)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(separator, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE).addGap(1)
						.addComponent(panel_6, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE).addGap(7)
						.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE).addGap(15)
						.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)));
		panel_6.setLayout(new GridLayout(2, 0, 0, 0));

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBackground(new Color(255, 255, 255));
		panel_6.add(lblPassword);
		lblPassword.setFont(new Font("Verdana", Font.BOLD, 14));
		lblPassword.setForeground(new Color(255, 255, 255));

		edtPassword = new JPasswordField();
		edtPassword.setFont(new Font("Verdana", Font.BOLD, 12));
		edtPassword.setForeground(new Color(255, 255, 255));
		edtPassword.setBorder(null);
		edtPassword.setBackground(new Color(85, 142, 203));
		panel_6.add(edtPassword);
		panel_1.setLayout(new GridLayout(2, 0, 0, 0));

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBackground(new Color(255, 255, 255));
		panel_1.add(lblUsername);
		lblUsername.setFont(new Font("Verdana", Font.BOLD, 14));
		lblUsername.setForeground(new Color(255, 255, 255));

		edtUsername = new JTextField();
		panel_1.add(edtUsername);
		edtUsername.setBorder(null);
		edtUsername.setForeground(new Color(255, 255, 255));
		edtUsername.setFont(new Font("Verdana", Font.BOLD, 12));
		edtUsername.setBackground(new Color(85, 142, 203));
		edtUsername.setColumns(10);
		panel_5.setLayout(gl_panel_5);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					CaturaBackEnd window = new CaturaBackEnd();
					window.pnlLogin.setResizable(false);
					window.pnlLogin.setLocationRelativeTo(null);

					window.pnlLogin.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnLogin) {

			try {

				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");

				if (edtUsername.getText().equals("Leske") && edtPassword.getText().equals("Catura$")) {

					pnlLogin.dispose();
					Menu menu = new Menu();
					menu.pnlMenu.setVisible(true);
					menu.setExtendedState(JFrame.MAXIMIZED_BOTH);
					menu.setLocationRelativeTo(null);
					menu.pnlMenu.setVisible(true);

				} else if (edtUsername.getText().equals("Natalie") && edtPassword.getText().equals("Ionpaint0924")) {

					pnlLogin.dispose();
					Menu menu = new Menu();
					menu.pnlMenu.setVisible(true);
					menu.setExtendedState(JFrame.MAXIMIZED_BOTH);
					menu.setLocationRelativeTo(null);
					menu.pnlMenu.setVisible(true);

				} else {

					JOptionPane.showMessageDialog(null, "Username or Passoword is incorrect");

				}

			} catch (Exception a) {
				// TODO Auto-generated catch block
				a.printStackTrace();
			}

		}

	}
}
